#include "widgetclient.h"
#include <QTabWidget>
#include <QQueue>
#include <QSplitter>
#include <QAction>

//------------------------------------------------------------------------------
WidgetClient::WidgetClient(QWidget *parent) :
    QMainWindow(parent)
{
    createWidgets();
}

//------------------------------------------------------------------------------
void WidgetClient::closeProgram()
{
    emit signalCloseProgram();
}

//------------------------------------------------------------------------------
void WidgetClient::slotSetFont(const QFont &currentFont)
{
    font = currentFont;
    for (int i = 0; i < tabEditor->count(); ++i) {
        TextEditor *editor = qobject_cast<TextEditor *>(tabEditor->widget(i));
        if (editor) {
            editor->setFont(font);
        }
    }
}

//------------------------------------------------------------------------------
void WidgetClient::slotSetConnections(QList<ConnectData *> listConnections)
{
    tabConnect->clear();
    for (int i = 0; i < listConnections.size(); ++i) {
        Client *client = new Client;
        tabConnect->addTab(client, listConnections.at(i)->objectName());
        client->setConnectData(listConnections.at(i));
        client->setCurrentFont(font);
        connect(client, SIGNAL(signalRequireCommands()),
                SLOT(slotQueueCommands()));
        connect(client, SIGNAL(signalConnected()),
                SIGNAL(signalConnected()));
        connect(client, SIGNAL(signalDisconnected()),
                SIGNAL(signalDisconnected()));
        connect(this, SIGNAL(signalConnectionDisable()),
                client, SLOT(slotConnectionDisable()));
        connect(this, SIGNAL(signalConnectionEnable()),
                client, SLOT(slotConnectionEnable()));
        connect(client, SIGNAL(signalProgressBarReset()),
                SIGNAL(signalProgressBarReset()));
        connect(client, SIGNAL(signalProgressBarSetRange(int)),
                SIGNAL(signalProgressBarSetRange(int)));
        connect(client, SIGNAL(signalProgressBarNextStep()),
                SIGNAL(signalProgressBarNextStep()));
    }
}

//------------------------------------------------------------------------------
void WidgetClient::slotConnected()
{
    emit signalConnectionDisable();
}

//------------------------------------------------------------------------------
void WidgetClient::slotDisconnected()
{
    emit signalConnectionEnable();
}

//------------------------------------------------------------------------------
void WidgetClient::slotSetTabTitle(const QString &name)
{
    TextEditor *editor = qobject_cast<TextEditor *>(tabEditor->currentWidget());
    if (editor) {
        connect(editor, SIGNAL(signalTextChanged(QPair<QString,bool>)),
                SIGNAL(signalTextChanged(QPair<QString,bool>)));
        tabEditor->setTabText(tabEditor->currentIndex(), name);
    }
}

//------------------------------------------------------------------------------
void WidgetClient::slotQueueCommands()
{
    QRegExp re_sign("^#");
    QRegExp re_comment_cpp("^/{2,}");

    TextEditor *editor = qobject_cast<TextEditor *>(tabEditor->currentWidget());
    if (editor) {
        QString str = editor->selectedText();
        QStringList slist = str.split(QChar(0x2029));

        QQueue<QString> queCommand;
        foreach (QString lstr, slist) {
            if (lstr.contains(re_sign) or lstr.contains(re_comment_cpp))
                continue;
            lstr.resize(lstr.indexOf(";") + 1);
            lstr.append('\n');
            queCommand.enqueue(lstr.toUpper());
        }

        Client *client = qobject_cast<Client *>(tabConnect->currentWidget());
        if (client) {
            client->runQueueCommands(queCommand);
        }
    }
}

//------------------------------------------------------------------------------
void WidgetClient::slotAddTab()
{
    TextEditor *editor = new TextEditor(tabEditor);
    editor->setFont(font);
    tabEditor->addTab(editor, "untitled");
    tabEditor->setCurrentWidget(editor);
    connect(editor, SIGNAL(signalWindowTitle(QString)),
            this, SLOT(slotSetTabTitle(QString)));
    connect(this, SIGNAL(signalCloseProgram()),
            editor, SLOT(slotSave()));
}

//------------------------------------------------------------------------------
void WidgetClient::slotDelTab(int index)
{
    tabEditor->setCurrentIndex(index);
    deleteTab(index);
}

//------------------------------------------------------------------------------
void WidgetClient::slotDelTab()
{
    int index = tabEditor->currentIndex();
    deleteTab(index);
}

//------------------------------------------------------------------------------
void WidgetClient::deleteTab(int index)
{
    if (tabEditor->count() > 1) {
        TextEditor *editor = qobject_cast<TextEditor *>(tabEditor->currentWidget());
        if (editor) {
            if (editor->slotClose()) {
                tabEditor->removeTab(index);
                editor->deleteLater();
            }
        }
    }
}

//------------------------------------------------------------------------------
void WidgetClient::createWidgets()
{
    tabConnect = new QTabWidget;
    QAction *actionAddTab = new QAction(this);
    QAction *actionDelTab = new QAction(this);
    actionAddTab->setShortcut(QKeySequence::AddTab);
    actionDelTab->setShortcut(QKeySequence("Ctrl+W"));
    tabEditor = new QTabWidget;
    tabEditor->setTabsClosable(true);
    tabEditor->addAction(actionAddTab);
    tabEditor->addAction(actionDelTab);
    slotAddTab();

    connect(actionAddTab, SIGNAL(triggered()), SLOT(slotAddTab()));
    connect(actionDelTab, SIGNAL(triggered()), SLOT(slotDelTab()));
    connect(tabEditor, SIGNAL(tabCloseRequested(int)), SLOT(slotDelTab(int)));
    
    QSplitter *splitter = new QSplitter(Qt::Vertical, this);
    splitter->addWidget(tabEditor);
    splitter->addWidget(tabConnect);
    
    setCentralWidget(splitter);
}
