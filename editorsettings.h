#ifndef EDITORSETTINGS_H
#define EDITORSETTINGS_H

#include <QWidget>

class QFontComboBox;
class QComboBox;

class EditorSettings : public QWidget
{
    Q_OBJECT
public:
    explicit EditorSettings(QWidget *parent = 0);

public:
    QFont getCurrentFont();
    void setCurrentFont(const QFont &font);

signals:
    void signalCurrentFont(const QFont &currentFont);
    void signalChangeSettings(int);

public slots:
    void slotApplySettings();

private:
    void createWidgets();
    void createConnect();
    void addItemsComboBoxFontSize();

private:
    QFontComboBox *comboBoxFont;
    QComboBox *comboBoxFontSize;
    QFont currentFont;

};

#endif // EDITORSETTINGS_H
