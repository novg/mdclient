#include "basesql.h"
#include <QSqlQueryModel>
#include <QSqlError>
#include <QSqlQuery>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QBoxLayout>
#include <QSplitter>

//------------------------------------------------------------------------------
BaseSql::BaseSql(QWidget *parent) :
    QWidget(parent)
{
    createWidgets();
    connect(pushSql, SIGNAL(clicked()), SLOT(slotShowTable()));
}

//------------------------------------------------------------------------------
void BaseSql::slotShowTable()
{
    QSqlQueryModel *model = new QSqlQueryModel(this);
    QString strQuery = tSqlQuery->toPlainText();
    model->setQuery(strQuery);

    if (model->lastError().isValid()) {
        errorEdit->setText(model->lastError().text());
    } else {
        errorEdit->clear();
    }

    view->setModel(model);

    if (strQuery.contains(QRegExp(";\\s*$")))
        strQuery.remove(QRegExp(";\\s*$"));
    strQuery = "(" + strQuery + ")";

    QSqlQuery query(QString("SELECT COUNT(*) FROM %1;").arg(strQuery));
    query.first();

    labelRows->setText(tr("Rows: %1").arg(query.value(0).toInt()));
}

//------------------------------------------------------------------------------
void BaseSql::createWidgets()
{
    QWidget *sqlTab = new QWidget;
    view = new QTableView;
    labelRows = new QLabel("Rows: 0");
    errorEdit        = new QLineEdit;
    pushSql          = new QPushButton(tr("Execute query"));
    tSqlQuery        = new QTextEdit;
    QLabel *lSql     = new QLabel(tr("SQL string:"));
    QLabel *errorSql = new QLabel(tr("Error message from database engine:"));


    //Layouts sqlTab
    QHBoxLayout *phbxPushLayout = new QHBoxLayout;
    phbxPushLayout->addWidget(pushSql);
    phbxPushLayout->addWidget(labelRows);
    phbxPushLayout->addStretch(1);

    QVBoxLayout *pvbxSQLayout = new QVBoxLayout;
    pvbxSQLayout->addWidget(lSql);
    pvbxSQLayout->addWidget(tSqlQuery);
    pvbxSQLayout->addLayout(phbxPushLayout);
    pvbxSQLayout->addWidget(errorSql);
    pvbxSQLayout->addWidget(errorEdit);
    sqlTab->setLayout(pvbxSQLayout);

    //
    QSplitter *splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(sqlTab);
    splitter->addWidget(view);
    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 10);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(splitter);
    setLayout(layout);
}
