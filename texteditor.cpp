#include "texteditor.h"
#include "syntaxhighlighter.h"
#include <QPlainTextEdit>
#include <QToolBar>
#include <QAction>
#include <QDebug>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QTextDocumentWriter>
#include <QFile>
#include <QTextCodec>

//------------------------------------------------------------------------------
TextEditor::TextEditor(QWidget *parent) :
    QMainWindow(parent)
{
    textEdit = new QPlainTextEdit;
    new SyntaxHighlighter(textEdit->document());

    setCentralWidget(textEdit);

    createActions();
    createConnect();
    setCurrentFile("");
}

//------------------------------------------------------------------------------
TextEditor::~TextEditor()
{
    documentChange.second = false;
    emit signalTextChanged(documentChange);
}

//------------------------------------------------------------------------------
QString TextEditor::selectedText() const
{
    return textEdit->textCursor().selectedText();
}

//------------------------------------------------------------------------------
void TextEditor::setFont(const QFont &font)
{
    textEdit->document()->setDefaultFont(font);
}

//------------------------------------------------------------------------------
void TextEditor::slotOpen()
{
    if (maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), tr(""),
                       tr("Text files (*.txt);;All files (*)"));
        if (!fileName.isEmpty()) {
            loadFile(fileName);
        }
    }
}

//------------------------------------------------------------------------------
void TextEditor::slotSaveAs()
{
    if (saveAs()) {
        actionSave->setEnabled(false);
    }
}

//------------------------------------------------------------------------------
void TextEditor::slotSave()
{
    if (save()) {
        actionSave->setEnabled(false);
    }
}

//------------------------------------------------------------------------------
bool TextEditor::saveFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Error"),
                             tr("Could not write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString())
                             );
        return false;
    }

    QTextStream out(&file);
    out << textEdit->toPlainText();
    setCurrentFile(fileName);
    file.close();
    return true;
}

//------------------------------------------------------------------------------
void TextEditor::winTitle()
{
    QString fname;
    if (curFile.isEmpty()) {
        fname = "untitled";
    } else {
        QFileInfo fi(curFile);
        fname = fi.fileName();
        setObjectName(fname);
    }

    emit signalWindowTitle(fname);
}

//------------------------------------------------------------------------------
void TextEditor::setCurrentFile(const QString &fileName)
{
    curFile = fileName;
    textEdit->document()->setModified(false);
//    setWindowModified(false);
    winTitle();
}

//------------------------------------------------------------------------------
bool TextEditor::maybeSave()
{
    if (textEdit->document()->isModified()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("MDClient"),
                                   tr("The document has been modified.\n"
                                      "Do you want to save your changes ?"),
                                   QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel
                                   );
        if (ret == QMessageBox::Save) {
            return save();
        } else if (ret == QMessageBox::Cancel) {
            return false;
        }
    }

    return true;
}

//------------------------------------------------------------------------------
bool TextEditor::save()
{
    if (curFile.isEmpty()) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}

//------------------------------------------------------------------------------
bool TextEditor::saveAs()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Text files (*.txt);;All files (*)"));
    dialog.exec();

    QStringList files = dialog.selectedFiles();
    if (files.isEmpty())
        return false;

    return saveFile(files.at(0));
}

//------------------------------------------------------------------------------
void TextEditor::loadFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
       QMessageBox::warning(this, tr("Error"), tr("Cannot read file %1:\n%2")
                            .arg(fileName)
                            .arg(file.errorString())
                            );
       return;
    }

    QTextStream in(&file);
    textEdit->setPlainText(in.readAll());
    file.close();
    setCurrentFile(fileName);
    actionSave->setEnabled(false);
}

//------------------------------------------------------------------------------
bool TextEditor::slotClose()
{
    if (maybeSave()) {
        setCurrentFile("");
        textEdit->clear();
        actionSave->setEnabled(false);
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
void TextEditor::slotShowSaveAction()
{
    actionSave->setEnabled(true);
}

//------------------------------------------------------------------------------
void TextEditor::slotFindDialog()
{
    findDialog->show();
}

//------------------------------------------------------------------------------
void TextEditor::slotFindNext(const QString &str, Qt::CaseSensitivity cs)
{
    if (cs == Qt::CaseInsensitive) {
        textEdit->find(str);
    } else if (cs == Qt::CaseSensitive) {
        textEdit->find(str, QTextDocument::FindCaseSensitively);
    }
}

//------------------------------------------------------------------------------
void TextEditor::slotFindPrev(const QString &str, Qt::CaseSensitivity cs)
{
    if (cs == Qt::CaseInsensitive) {
        textEdit->find(str, QTextDocument::FindBackward);
    } else if (cs == Qt::CaseSensitive) {
        textEdit->find(str, QTextDocument::FindBackward |
                       QTextDocument::FindCaseSensitively);
    }
}

//------------------------------------------------------------------------------
void TextEditor::slotTextChanged(bool change)
{
    documentChange.first = objectName();
    documentChange.second = change;
    emit signalTextChanged(documentChange);
}

//------------------------------------------------------------------------------
void TextEditor::slotAbout()
{
    QMessageBox::about(0, "About", "<h1><b><i><u>MDClient ver. 0.8</u></i></b></h1>"
                       "powered by: A. Novgorodskiy<br/>"
//                       "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                       "email: <a href=\"mailto:novg.novg@gmail.com\">novg.novg@gmail.com</a><br/>"
                       );
}

//------------------------------------------------------------------------------
void TextEditor::createActions()
{
    actionOpen = new QAction(tr("&Open"), this);
    actionOpen->setShortcut(QKeySequence("Ctrl+O"));
    actionOpen->setToolTip(tr("Open document... <i><b>Ctrl+O</b></i>"));
    actionOpen->setStatusTip(tr("Open a file in editor"));
    actionOpen->setWhatsThis(tr("Open a file in editor"));
    actionOpen->setIcon(QIcon(":/images/folder_open.png"));

    actionSave = new QAction(tr("&Save"), this);
    actionSave->setShortcut(QKeySequence("Ctrl+S"));
    actionSave->setToolTip(tr("Save document <i><b>Ctrl+S</b></i>"));
    actionSave->setStatusTip(tr("Save the file to disk"));
    actionSave->setWhatsThis(tr("Save the file to disk"));
    actionSave->setIcon(QIcon(":/images/save.png"));
    actionSave->setEnabled(false);

    actionSaveAs = new QAction(tr("&Save as..."), this);
    actionSaveAs->setToolTip(tr("Save document as..."));
    actionSaveAs->setStatusTip(tr("Save the file to disk as..."));
    actionSaveAs->setWhatsThis(tr("Save the file to disk as..."));
    actionSaveAs->setIcon(QIcon(":/images/save_as.png"));

    actionUndo = new QAction(tr("&Undo"), this);
    actionUndo->setToolTip(tr("Undo <i><b>Ctrl+Z</b></i>"));
    actionUndo->setStatusTip(tr("Undo"));
    actionUndo->setWhatsThis(tr("Undo"));
    actionUndo->setIcon(QIcon(":/images/undo.png"));
    actionUndo->setEnabled(false);

    actionredo = new QAction(tr("&Redo"), this);
    actionredo->setToolTip(tr("Redo <i><b>Ctrl+Y</b></i>"));
    actionredo->setStatusTip(tr("Redo"));
    actionredo->setWhatsThis(tr("Redo"));
    actionredo->setIcon(QIcon(":/images/redo.png"));
    actionredo->setEnabled(false);

    actionCut = new QAction(tr("&Cut"), this);
    actionCut->setToolTip(tr("Cut <i><b>Ctrl+X</b></i>"));
    actionCut->setStatusTip(tr("Cut"));
    actionCut->setWhatsThis(tr("Cut"));
    actionCut->setIcon(QIcon(":/images/cut.png"));

    actionCopy = new QAction(tr("&Copy"), this);
    actionCopy->setToolTip(tr("Copy <i><b>Ctrl+C</b></i>"));
    actionCopy->setStatusTip(tr("Copy"));
    actionCopy->setWhatsThis(tr("Copy"));
    actionCopy->setIcon(QIcon(":/images/copy.png"));

    actionPaste = new QAction(tr("&Paste"), this);
    actionPaste->setToolTip(tr("Paste <i><b>Ctrl+V</b></i>"));
    actionPaste->setStatusTip(tr("Paste"));
    actionPaste->setWhatsThis(tr("Paste"));
    actionPaste->setIcon(QIcon(":/images/paste.png"));

    actionFind = new QAction(tr("&Find"), this);
    actionFind->setShortcut(QKeySequence("Ctrl+F"));
    actionFind->setToolTip(tr("Find text <i><b>Ctrl+F</b></i>"));
    actionFind->setStatusTip(tr("Find a text in file"));
    actionFind->setWhatsThis(tr("Find a text in file"));
    actionFind->setIcon(QIcon("://images/search.png"));

    actionAbout = new QAction(tr("&About"), this);
    actionAbout->setToolTip(tr("About"));
    actionAbout->setStatusTip(tr("About program"));
    actionAbout->setWhatsThis(tr("About program"));
    actionAbout->setIcon(QIcon(":/images/info_blue.png"));

    toolBar = addToolBar(tr("Text Editor toolBar"));
    toolBar->addAction(actionOpen);
    toolBar->addAction(actionSave);
    toolBar->addAction(actionSaveAs);
    toolBar->addSeparator();
    toolBar->addAction(actionFind);
    toolBar->addAction(actionUndo);
    toolBar->addAction(actionredo);
    toolBar->addAction(actionCut);
    toolBar->addAction(actionCopy);
    toolBar->addAction(actionPaste);
    toolBar->addSeparator();
    toolBar->addAction(actionAbout);

    findDialog = new FindDialod(this);
}

//------------------------------------------------------------------------------
void TextEditor::createConnect()
{
    connect(actionOpen, SIGNAL(triggered()), SLOT(slotOpen()));
    connect(actionSave, SIGNAL(triggered()), SLOT(slotSave()));
    connect(actionSaveAs, SIGNAL(triggered()), SLOT(slotSaveAs()));
    connect(textEdit, SIGNAL(textChanged()), SLOT(slotShowSaveAction()));
    connect(actionUndo, SIGNAL(triggered()), textEdit, SLOT(undo()));
    connect(actionredo, SIGNAL(triggered()), textEdit, SLOT(redo()));
    connect(textEdit, SIGNAL(undoAvailable(bool)),
            actionUndo, SLOT(setEnabled(bool)));
    connect(textEdit, SIGNAL(redoAvailable(bool)),
            actionredo, SLOT(setEnabled(bool)));
    connect(actionCut, SIGNAL(triggered()), textEdit, SLOT(cut()));
    connect(actionCopy, SIGNAL(triggered()), textEdit, SLOT(copy()));
    connect(actionPaste, SIGNAL(triggered()), textEdit, SLOT(paste()));
    connect(actionFind, SIGNAL(triggered()), SLOT(slotFindDialog()));
    connect(actionAbout, SIGNAL(triggered()), SLOT(slotAbout()));
    connect(findDialog, SIGNAL(findNext(QString,Qt::CaseSensitivity)),
            this, SLOT(slotFindNext(QString,Qt::CaseSensitivity)));
    connect(findDialog, SIGNAL(findPrev(QString,Qt::CaseSensitivity)),
            this, SLOT(slotFindPrev(QString,Qt::CaseSensitivity)));
//    connect(textEdit->document(), SIGNAL(modificationChanged(bool)),
//            SIGNAL(signalTextChanged(bool)));
    connect(textEdit->document(), SIGNAL(modificationChanged(bool)),
            SLOT(slotTextChanged(bool)));
}
