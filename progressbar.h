#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QWidget>
class QProgressBar;

class ProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit ProgressBar(QWidget *parent = 0);

signals:

public slots:
    void slotInit(const int &num);
    void slotNextStep();
    void slotReset();

private:
    void createWidgets();

private:
    QProgressBar *progressBar;
    int step;
    int range;

};

#endif // PROGRESSBAR_H
