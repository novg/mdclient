#include "basetables.h"

#include <QDebug>
#include <QFileInfo>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QBoxLayout>
#include <QFileDialog>
#include <QFile>

//------------------------------------------------------------------------------
BaseTables::BaseTables(QWidget *parent) :
    QWidget(parent)
{
    tableCross    = "cross";
    tableComments = "comments";
    tableNumplan  = "numplan";
    tableDirnum   = "dirnum";
    tableAbbnum   = "abbnum";
    tableNamenum  = "namenum";
    viewTotal     = "total";
    viewFreeEquMd110  = "freeEquMD110";
    viewFreeEquAastra = "freeEquAastra";
    viewFreeNumbers   = "freeNum";

    clearList = (QStringList()
                 << tableDirnum
                 << tableAbbnum
                 << tableNamenum
                );

    allow = false;
    path = "dbase";
    dbname = path + "/atsbase.db";
    dbConnect = createDataBaseConnect();
    dbConnect = createTables();
    createWidgets();

    connect(viewSqlTable, SIGNAL(signalLoadFile(QString)),
            SLOT(slotFillTables(QString)));
}

//------------------------------------------------------------------------------
BaseTables::~BaseTables()
{
    delete db;
}

void BaseTables::setCountConnections(const int count)
{
    countConnections = count;
}

void BaseTables::setAllow()
{
    allow = true;
}

//------------------------------------------------------------------------------
void BaseTables::slotReceiveLogName(const QString &fileName)
{
    static int count = 0;
    ++count;

    if (count <= countConnections)
        logFiles << fileName;

    if ((count % countConnections == 0) && allow)
        baseUpdate();
}

//------------------------------------------------------------------------------
void BaseTables::slotFillTables(const QString &table)
{
    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Open file"), "", "");

    if (fileName.isEmpty())
        return;

    QFile currentFile(fileName);
    if (!currentFile.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "Error opening file:" << fileName;
        return;
    }

    if (dbConnect) {
        if (!mapTables[table]->clearTable())
            return;

        db->transaction();
        bool sqlOk = true;
        while(!currentFile.atEnd()) {
            QString line = currentFile.readLine();
            line.replace("\n", "");
            QStringList list = line.split(" ", QString::SkipEmptyParts);

            if (!mapTables[table]->insert(list)) {
                sqlOk = false;
                break;
            }
        }

        if (sqlOk == true)
            sqlOk = db->commit();

        if (sqlOk == false) {
            qDebug() << "Error: transaction refuse";
            db->rollback();
        }
    }

    currentFile.close();
}

//------------------------------------------------------------------------------
void BaseTables::createWidgets()
{
    viewSqlTable = new ViewSqlTable(db);
    QVBoxLayout *vboxlayout = new QVBoxLayout;
    vboxlayout->addWidget(viewSqlTable);
    setLayout(vboxlayout);
}

//------------------------------------------------------------------------------
bool BaseTables::createTables()
{
    if (!initTables(tableCross,
                    "CREATE TABLE IF NOT EXISTS %1 ( "
                    "equ VARCHAR(12), "
                    "cross VARCHAR(8), "
                    "type VARCHAR(5), "
                    "station VARCHAR(8), "
                    "lim INTEGER, "
                    "state VARCHAR(20) "
                    ");",
                    "INSERT INTO %1 (equ, cross, type, station, lim, state) "
                    "VALUES('%2', '%3', '%4', '%5', %6, '%7');"
                    )) {
        return false;
    }

    if (!initTables(tableComments,
                    "CREATE TABLE IF NOT EXISTS %1 ( "
                    "dir INTEGER, "
                    "comment VARCHAR(50) "
                    ");",
                    "INSERT INTO %1 (dir, comment) "
                    "VALUES(%2, '%3');"
                   )) {
        return false;
    }

    if (!initTables(tableNumplan,
                    "CREATE TABLE IF NOT EXISTS %1 ( "
                    "dir INTEGER"
                    ");",
                    "INSERT INTO %1 (dir) VALUES(%2);"
                   )) {
        return false;
    }

    if (!initTables(tableDirnum,
                    "CREATE TABLE IF NOT EXISTS %1 ( "
                    "dir INTEGER, "
                    "cust INTEGER, "
                    "equ VARCHAR(12), "
                    "cat INTEGER, "
                    "type VARCHAR(5), "
                    "station VARCHAR(8) "
                    ");",
                    "INSERT INTO %1 (dir, cust, equ, cat, type, station) "
                    "VALUES(%2, %3, '%4', %5, '%6', '%7');"
                   )) {
        return false;
    }

    if (!initTables(tableAbbnum,
                    "CREATE TABLE IF NOT EXISTS %1 ( "
                    "abb VARCHAR(20), "
                    "tra INTEGER, "
                    "class VARCHAR(5), "
                    "station VARCHAR(8) "
                    ");",
                    "INSERT INTO %1 (abb, tra, class, station) "
                    "VALUES('%2', %3, '%4', '%5');"
                   )) {
        return false;
    }

    if (!initTables(tableNamenum,
                    "CREATE TABLE IF NOT EXISTS %1 ( "
                    "dir INTEGER, "
                    "name1 VARCHAR(25), "
                    "name2 VARCHAR(25), "
                    "station VARCHAR(8) "
                    ");",
                    "INSERT INTO %1 (dir, name1, name2, station) "
                    "VALUES(%2, '%3', '%4', '%5');"
                   )) {
        return false;
    }

    if (!initTables(viewTotal,
                    QString(
                        "CREATE VIEW IF NOT EXISTS %5 AS SELECT DISTINCT "
                        "d.dir, d.cust, d.cat, d.type, d.station, "
                        "k.equ, k.cross, k.lim, "
                        "n.name1, n.name2, c.comment "
                        "FROM %1 d JOIN %2 k ON (d.equ = k.equ AND d.station = k.station) "
                        "LEFT OUTER JOIN %3 n ON (d.dir = n.dir) "
                        "LEFT OUTER JOIN %4 c ON (d.dir = c.dir)"
                        ";"
                        )
                    .arg(tableDirnum)
                    .arg(tableCross)
                    .arg(tableNamenum)
                    .arg(tableComments)
                    ,
                    "%1"
                   )) {
        return false;
    }

    if (!initTables(viewFreeEquMd110,
                    QString(
                        "CREATE VIEW IF NOT EXISTS %3 AS "
                        "SELECT * FROM %1 "
                        "WHERE station='md110'"
                        "AND equ NOT IN "
                        "(SELECT equ FROM %2 WHERE station='md110')"
                        ";"
                        )
                    .arg(tableCross)
                    .arg(tableDirnum)
                    ,
                    "%1"
                   )) {
        return false;
    }

    if (!initTables(viewFreeEquAastra,
                    QString(
                        "CREATE VIEW IF NOT EXISTS %3 AS "
                        "SELECT * FROM %1 "
                        "WHERE station='aastra'"
                        "AND equ NOT IN "
                        "(SELECT equ FROM %2 WHERE station='aastra')"
                        ";"
                        )
                    .arg(tableCross)
                    .arg(tableDirnum)
                    ,
                    "%1"
                   )) {
        return false;
    }

    if (!initTables(viewFreeNumbers,
                    QString(
                        "CREATE VIEW IF NOT EXISTS %4 AS "
                        "SELECT * FROM %1 "
                        "WHERE dir NOT IN "
                        "(SELECT dir FROM %2)"
                        "AND dir NOT IN "
                        "(SELECT abb FROM %3)"
                        ";"
                        )
                    .arg(tableNumplan)
                    .arg(tableDirnum)
                    .arg(tableAbbnum)
                    ,
                    "%1"
                   )) {
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
bool BaseTables::initTables(const QString &name, const QString &create, const QString &insert)
{
    Table *tempTable = new Table(name, query, this);
    if (!tempTable->create(create))
        return false;
    tempTable->setSqlInsert(insert);
    mapTables.insert(name, tempTable);

    return true;
}

//------------------------------------------------------------------------------
bool BaseTables::createDataBaseConnect()
{
    db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    db->setDatabaseName(dbname);

    if (!db->open()) {
        qDebug() << "Cannot open database:" << dbname << db->lastError();
        return false;
    }
    query = QSqlQuery(*db);

    return true;
}

//------------------------------------------------------------------------------
void BaseTables::baseUpdate()
{
    if (dbConnect) {
        db->transaction();
        bool sqlOk = true;
        if (clearTables()) {
            for (auto fname : logFiles) {
                if(!fillTables(fname)) {
                    sqlOk = false;
                    break;
                }
            }
        } else {
            sqlOk = false;
        }

        if (sqlOk == true) {
            sqlOk = db->commit();
        }

        if (sqlOk == false) {
            qDebug() << "Error: transaction refuse";
            db->rollback();
        }
    }
}

//------------------------------------------------------------------------------
bool BaseTables::fillTables(const QString &fileName)
{
    QFileInfo info(fileName);
    QString baseName = info.baseName();

    QFile currentFile(fileName);
    if (!currentFile.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "Error opening file:" << fileName;
    }

    QString flag;
    bool insert = true;

    while (!currentFile.atEnd()) {
        QString line = currentFile.readLine();
        line.replace("\n", "");

        if (line.contains("EXDDP")) flag = "EXDDP";
        if (line.contains("KSDDP")) flag = "KSDDP";
        if (line.contains("GEDIP")) flag = "GEDIP";
        if (line.contains("ADCDP")) flag = "ADCDP";
        if (line.contains("NIINP")) flag = "NIINP";

        if (line[0].isDigit()) {
            QStringList values = line.split(" ", QString::SkipEmptyParts);
            // Преобразования строк для записи аналоговых портов в БД.
            if (flag == "EXDDP") {
                values.removeLast();
                // Если отсутствует категория организации абонента (CUST).
                if (values[1] == "-")
                    values[1] = "NULL";
                // Если отсутствует категория абонента (CAT).
                if (values[3] == "-")
                    values[3] = "NULL";
                values.append(baseName);
                if (!mapTables[tableDirnum]->insert(values)) {
                    insert = false;
                    break;
                }
            }
            // Преобразования строк для записи цифровых портов в БД.
            if (flag == "KSDDP") {
                values[values.size() - 1] = "KL1";
                // Если отсутствует категория организации абонента (CUST).
                if (values[1] == "-")
                    values[1] = "NULL";
                // Если отсутствует категория абонента (CAT).
                if (values[3] == "-")
                    values[3] = "NULL";
                values.append(baseName);
                if (!mapTables[tableDirnum]->insert(values)) {
                    insert = false;
                    break;
                }
            }
            // Преобразования строк для записи DECT портов в БД.
            if (flag == "GEDIP") {
                values.insert(1, values[3]);
                values.removeLast();
                if (values.size() > 5) {
                    values.removeLast();
                    values.removeLast();
                }
                values[values.size() - 1] = "DECT";
                values[2] = "LIM" + values[2];
                // Если отсутствует категория организации абонента (CUST).
                if (values[1] == "-")
                    values[1] = "NULL";
                // Если отсутствует категория абонента (CAT).
                if (values[3] == "-")
                    values[3] = "NULL";
                values.append(baseName);
                if (!mapTables[tableDirnum]->insert(values)) {
                    insert = false;
                    break;
                }
            }
            // Преобразования строк для записи сокращённых номеров в БД.
            if (flag == "ADCDP") {
                if (values.size() > 3)
                    values.removeLast();
                values.append(baseName);
                if (!mapTables[tableAbbnum]->insert(values)) {
                    insert = false;
                    break;
                }
            }
            // Преобразования строк для записи имён в БД.
            if (flag == "NIINP") {
                values.removeLast();
                if (values.size() < 3)
                    values.append("");
                values.append(baseName);
                if (!mapTables[tableNamenum]->insert(values)) {
                    insert = false;
                    break;
                }
            }
        }
    }

    currentFile.close();
    return insert;
}

//------------------------------------------------------------------------------
bool BaseTables::clearTables()
{
//    for (auto table : mapTables) {
//        if (!table->clearTable())
//            return false;
//    }
    auto iter = mapTables.constBegin();
    while(iter != mapTables.constEnd()) {
        if (clearList.contains(iter.key()))
            iter.value()->clearTable();
        ++iter;
    }

    return true;
}
