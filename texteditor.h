#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include "finddialod.h"
#include <QMainWindow>
#include <QCloseEvent>

class QPlainTextEdit;
class QToolBar;
class QAction;

class TextEditor : public QMainWindow
{
    Q_OBJECT
public:
    explicit TextEditor(QWidget *parent = 0);
    ~TextEditor();

public:
    QString selectedText() const;
    void setFont(const QFont &font);

signals:
    void signalWindowTitle(const QString &title);
    void signalTextChanged(const QPair<QString, bool> &documentChange);

public slots:
    bool slotClose();
    void slotSave();

private slots:
    void slotOpen();
    void slotSaveAs();
    void slotShowSaveAction();
    void slotFindDialog();
    void slotFindNext(const QString &str, Qt::CaseSensitivity cs);
    void slotFindPrev(const QString &str, Qt::CaseSensitivity cs);
    void slotTextChanged(bool change);
    void slotAbout();

private:
    void createActions();
    void createConnect();
    bool saveFile(const QString &fileName);
    void winTitle();
    void setCurrentFile(const QString &fileName);
    bool maybeSave();
    bool save();
    bool saveAs();
    void loadFile(const QString &fileName);

private:
    QPlainTextEdit *textEdit;
    QString    curFile;
    QToolBar   *toolBar;
    QAction    *actionOpen;
    QAction    *actionSaveAs;
    QAction    *actionSave;
    QAction    *actionUndo;
    QAction    *actionredo;
    QAction    *actionCut;
    QAction    *actionCopy;
    QAction    *actionPaste;
    QAction    *actionFont;
    QAction    *actionFind;
    QAction    *actionAbout;
    FindDialod *findDialog;
    QFont      font;
    QPair<QString, bool> documentChange;
};

#endif // TEXTEDITOR_H
