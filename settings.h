#ifndef SETTINGS_H
#define SETTINGS_H

#include "connectionsettings.h"
#include "editorsettings.h"
#include <QWidget>

class QPushButton;
class QSettings;

class Settings : public QWidget
{
    Q_OBJECT
public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

public:
    void getSettings();

signals:
    void signalCurrentFont(const QFont &currentFont);
    void signalConnectionData(QList<ConnectData *>);

public slots:

private slots:
    void slotChangeSettings(int);
    void slotApplySettings();

private:
    void createWidgets();
    void createConnect();
    void writeSettings();
    void readSettings();

private:
    ConnectionSettings *connectionSettings;
    EditorSettings *editorSettings;
    QPushButton *buttonApply;
    QSettings *settings;
    bool applySettings;
};

#endif // SETTINGS_H
