#include "connectdata.h"

//------------------------------------------------------------------------------
ConnectData::ConnectData(QObject *parent) :
    QObject(parent)
{
}

//------------------------------------------------------------------------------
void ConnectData::setAddress(const QString &addr)
{
    address = addr;
}

//------------------------------------------------------------------------------
void ConnectData::setPort(const QString &prt)
{
    port = prt;
}

//------------------------------------------------------------------------------
void ConnectData::setUserName(const QString &name)
{
    userName = name;
}

//------------------------------------------------------------------------------
void ConnectData::setPassword(const QString &passw)
{
    password = passw;
}

//------------------------------------------------------------------------------
QString ConnectData::getAddress() const
{
    return address;
}

//------------------------------------------------------------------------------
QString ConnectData::getPort() const
{
    return port;
}

//------------------------------------------------------------------------------
QString ConnectData::getUserName() const
{
    return userName;
}

//------------------------------------------------------------------------------
QString ConnectData::getPassword() const
{
    return password;
}
