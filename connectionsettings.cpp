#include "connectionsettings.h"
#include <QDebug>
#include <QPushButton>
#include <QBoxLayout>
#include <QInputDialog>
#include <QStackedWidget>

//------------------------------------------------------------------------------
ConnectionSettings::ConnectionSettings(QWidget *parent) :
    QWidget(parent)
{
    buttonWidth = 82;
    createWidgets();
    createConnect();
}

//------------------------------------------------------------------------------
void ConnectionSettings::slotDialogEnterName()
{
    bool pressOk = false;
    QString name = QInputDialog::getText(0,
                                         "Input name connect",
                                         "Name connect:",
                                         QLineEdit::Normal,
                                         "",
                                         &pressOk
                                         );
    if (pressOk) {
        emit signalTransmitConnectionName(name);
    }
}

//------------------------------------------------------------------------------
void ConnectionSettings::slotAddConnection(const QString &name)
{
    connectionName = new ConnectionName(name);
    layoutAddConnect->setRowStretch(layoutAddConnect->rowCount() - 1, 0);
    layoutAddConnect->addWidget(connectionName, layoutAddConnect->rowCount() - 1, 0);
    layoutAddConnect->setRowStretch(layoutAddConnect->rowCount(), 1);

    QWidget *widgetConnect= new QWidget;
    widgetConnect->setLayout(layoutAddConnect);

    stacketWidget->addWidget(widgetConnect);
    stacketWidget->setCurrentWidget(widgetConnect);

    connect(connectionName, SIGNAL(signalDeleteConnect(QString)),
            this, SLOT(slotDeleteConnection(QString)));

    listConnections << connectionName;
}

//------------------------------------------------------------------------------
void ConnectionSettings::slotApplySettings()
{
    QList<ConnectData *> listConnectData;
    for (int i = 0; i < listConnections.size(); ++i) {
        ConnectData *connectData = new ConnectData(this);
        connectData->setObjectName(listConnections.at(i)->objectName());
        connectData->setAddress(listConnections.at(i)->getAddress());
        connectData->setPort(listConnections.at(i)->getPort());
        connectData->setUserName(listConnections.at(i)->getUserName());
        connectData->setPassword(listConnections.at(i)->getPassword());
        listConnectData << connectData;
    }
    emit signalConnectionData(listConnectData);
}

//------------------------------------------------------------------------------
void ConnectionSettings::slotDeleteConnection(const QString &name)
{
    for (int i = 0; i < listConnections.size(); ++i) {
        if (listConnections.at(i)->objectName() == name) {
            listConnections.at(i)->deleteLater();
            listConnections.removeAt(i);
        }
    }
}

//------------------------------------------------------------------------------
void ConnectionSettings::createWidgets()
{
    buttonAddConnect = new QPushButton(tr("Add connection"));
    buttonAddConnect->setMaximumWidth(buttonWidth);

    layoutAddConnect = new QGridLayout;
    layoutAddConnect->addWidget(buttonAddConnect, 0, 0);
    layoutAddConnect->setColumnStretch(1, 1);
    layoutAddConnect->setRowStretch(1, 1);

    QWidget *widgetButton = new QWidget;
    widgetButton->setLayout(layoutAddConnect);

    stacketWidget = new QStackedWidget;
    stacketWidget->addWidget(widgetButton);
    stacketWidget->setCurrentWidget(widgetButton);

    QVBoxLayout *layoutConnect = new QVBoxLayout;
    layoutConnect->addWidget(stacketWidget);

    this->setLayout(layoutConnect);
}

//------------------------------------------------------------------------------
void ConnectionSettings::createConnect()
{
    connect(buttonAddConnect, SIGNAL(clicked()),
            this, SLOT(slotDialogEnterName()));
    connect(this, SIGNAL(signalTransmitConnectionName(QString)),
            this, SLOT(slotAddConnection(QString)));
}
