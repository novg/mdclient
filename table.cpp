#include "table.h"
#include <QDebug>
#include <QSqlError>

//------------------------------------------------------------------------------
Table::Table(const QString &tableName, QSqlQuery &sqlQuery, QObject *parent) :
    QObject(parent)
{
    name = tableName;
    query = sqlQuery;
}

//------------------------------------------------------------------------------
bool Table::create(const QString &strSql)
{
    QString strInsert = strSql.arg(name);
//    qDebug() << strInsert;
    if (!query.exec(strInsert)) {
        qDebug() << "Unable to create a table " << name;
//        qDebug() << strInsert;
//        qDebug() << "\n" << query.lastError();
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
bool Table::insert(const QStringList &values)
{
    QString str = sqlInsert;
    for(QString var : values)
        str = str.arg(var);

    if (!query.exec(str)) {
        qDebug() << str << "\n"
                 << query.lastError();
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
void Table::setSqlInsert(const QString &str)
{
    sqlInsert = str.arg(name);
}

//------------------------------------------------------------------------------
bool Table::clearTable()
{
    QString strDelete = "DELETE FROM %1";
    if (!query.exec(strDelete.arg(name))) {
        qDebug() << "Unable to make clear table" << name;
        return false;
    }

    return true;
}
