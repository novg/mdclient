#include "tcpclient.h"

//------------------------------------------------------------------------------
TcpClient::TcpClient(QObject *parent) :
    QObject(parent), socket(0)
{
    authorized = false;
    socket = new QTcpSocket;
    createConnect();
}

//------------------------------------------------------------------------------
TcpClient::~TcpClient()
{
    delete socket;
}

//------------------------------------------------------------------------------
void TcpClient::connectToHost(const QString &host, const QString &port)
{
    socket->connectToHost(host, port.toInt());
}

//------------------------------------------------------------------------------
void TcpClient::setConnectData(const QString &user, const QString &passwordString)
{
    userName = user;
    password = passwordString;
}

//------------------------------------------------------------------------------
void TcpClient::setQueueCommands(const QQueue<QString> &commands)
{
    queCommand = commands;
    runCommands();
}

//------------------------------------------------------------------------------
void TcpClient::emptyQueue()
{
    queCommand.clear();
}

//------------------------------------------------------------------------------
void TcpClient::slotDisconnectFromHost()
{
    authorized = false;
    socket->disconnectFromHost();
}

//------------------------------------------------------------------------------
void TcpClient::slotReadyRead()
{
    buffer.clear();
    buffer.append(socket->readAll());

    QString str = buffer.data();
    emit signalMessage(str);
}

//------------------------------------------------------------------------------
void TcpClient::sendToServer(const QString &text)
{
    QByteArray buff = text.toLocal8Bit();
    socket->write(buff);
}

//------------------------------------------------------------------------------
void TcpClient::slotError(QAbstractSocket::SocketError error)
{
    QString strError =
            "Error:" + (error == QAbstractSocket::HostNotFoundError ?
                            "The host was not found." :
                        error == QAbstractSocket::RemoteHostClosedError ?
                            "The remote host is closed." :
                        error == QAbstractSocket::ConnectionRefusedError ?
                            "The connection was refused." :
                        QString(socket->errorString())
            );
    buffer.append(strError);
}

//------------------------------------------------------------------------------
void TcpClient::slotReceiveFromServer(QString &text)
{
    QRegExp regexp("[�]");
    text.replace(regexp, "");
    text.replace("\r", "");

    if (text.contains("AUTHORITY CLASS")) {
        authorized = true;
    }

    if (authorized) {
        emit signalReceiveFromServer(text);
        if (text.contains("<")) {
            if (text.contains("SURE? (YES/NO)\n<")) {
                sendToServer("YES\n");
            }
            else {
                runCommands();
            }
        }
    } else {
        if (text.contains("login:")) {
            sendToServer("SSSSSSS;\n");
        }
        else if (text.contains("ENTER USER NAME")) {
            sendToServer(userName + '\n');
        }
        else if (text.contains("ENTER PASSWORD")) {
            sendToServer(password + '\n');
        }
    }
}

//------------------------------------------------------------------------------
void TcpClient::runCommands()
{
    if (!queCommand.isEmpty()) {
        sendToServer(queCommand.dequeue());
        emit signalProgressBarNextStep();
    } else {
        emit signalQueueCommandsEmpty();
    }
}

//------------------------------------------------------------------------------
void TcpClient::createConnect()
{
    connect(socket, SIGNAL(connected()), SIGNAL(signalConnected()));
    connect(socket, SIGNAL(disconnected()), SIGNAL(signalDisconnected()));
    connect(socket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
            SLOT(slotError(QAbstractSocket::SocketError)));
    connect(this, SIGNAL(signalMessage(QString&)),
            this, SLOT(slotReceiveFromServer(QString&)));
}
