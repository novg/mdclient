#ifndef VIEWSQLTABLE_H
#define VIEWSQLTABLE_H

#include <QWidget>
#include <QSqlDatabase>

class QComboBox;
class QLabel;
class QTableView;
class QStackedWidget;
class QSqlTableModel;
class QShortcut;
class QItemSelectionModel;

class ViewSqlTable : public QWidget
{
    Q_OBJECT
public:
    explicit ViewSqlTable(QSqlDatabase *dbase, QWidget *parent = 0);

signals:
    void signalLoadFile(const QString &name);

public slots:

private slots:
    void slotShowTable(const QString &table);
    void slotFilterTable();
    void slotClearFields();
    void slotLoadFile();
    void slotCopyToClipboard();

private:
    void createWidgets();
    void updateListTables();
    void createStackWidget(const QString &table, const QStringList &listColumns);
    void setLabelRows(const QString &request);
    QString filterColumn(const QString &column, const QString &request) const;

private:
    QStringList listLoadTables;
    QString currentTable;
    QString tableCross;
    QString tableComments;
    QString tableNumplan;
    QString tableDirnum;
    QString tableAbbnum;
    QString tableNamenum;
    QComboBox *comboTables;
    QLabel *labelRows;
    QTableView *view;
    QSqlDatabase *db;
    QStackedWidget *stackWidget;
    QSqlTableModel *model;
    QShortcut *copyToClipboard;
    QItemSelectionModel *selection;
};

#endif // VIEWSQLTABLE_H
