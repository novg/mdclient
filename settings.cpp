#include "settings.h"
#include "settingsbar.h"
#include <QDebug>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSettings>

//------------------------------------------------------------------------------
Settings::Settings(QWidget *parent) :
    QWidget(parent)
{
    settings = new QSettings("settings.ini", QSettings::IniFormat);
    createWidgets();
    createConnect();
    readSettings();
}

//------------------------------------------------------------------------------
Settings::~Settings()
{
    writeSettings();
    delete settings;
}

//------------------------------------------------------------------------------
void Settings::getSettings()
{
    editorSettings->slotApplySettings();
    connectionSettings->slotApplySettings();
}

//------------------------------------------------------------------------------
void Settings::slotChangeSettings(int)
{
    applySettings = false;
}

//------------------------------------------------------------------------------
void Settings::slotApplySettings()
{
    applySettings = true;
}

//------------------------------------------------------------------------------
void Settings::createWidgets()
{
    connectionSettings = new ConnectionSettings;
    editorSettings = new EditorSettings;
    SettingsBar *settingsBar = new SettingsBar(this);
    settingsBar->addTab(editorSettings, "Editor");
    settingsBar->addTab(connectionSettings, "Connections");

    buttonApply = new QPushButton(tr("Apply"));
    QHBoxLayout *layoutButton = new QHBoxLayout;
    layoutButton->addStretch(1);
    layoutButton->addWidget(buttonApply);

    QVBoxLayout *layoutGroup = new QVBoxLayout;
    layoutGroup->addWidget(settingsBar);
    layoutGroup->addLayout(layoutButton);
    setLayout(layoutGroup);
}

//------------------------------------------------------------------------------
void Settings::writeSettings()
{
    if (applySettings) {
        settings->beginWriteArray("connects");
        for (int i = 0; i < connectionSettings->listConnections.size(); ++i) {
            settings->setArrayIndex(i);
            settings->setValue("connectionName", connectionSettings->listConnections.at(i)->objectName());
            settings->setValue("address", connectionSettings->listConnections.at(i)->getAddress());
            settings->setValue("port", connectionSettings->listConnections.at(i)->getPort());
            settings->setValue("userName", connectionSettings->listConnections.at(i)->getUserName());
            settings->setValue("password", connectionSettings->listConnections.at(i)->getPassword());
        }
        settings->endArray();

        QFont currentFont = editorSettings->getCurrentFont();
        settings->setValue("Editor/font", currentFont);
    }
}

//------------------------------------------------------------------------------
void Settings::readSettings()
{
    int size = settings->beginReadArray("connects");
    for (int i = 0; i < size; ++i) {
        settings->setArrayIndex(i);
        QString name = settings->value("connectionName").toString();
        connectionSettings->slotAddConnection(name);
        connectionSettings->listConnections.at(i)->setAddress(settings->value("address").toString());
        connectionSettings->listConnections.at(i)->setPort(settings->value("port").toString());
        connectionSettings->listConnections.at(i)->setUserName(settings->value("userName").toString());
        connectionSettings->listConnections.at(i)->setPassword(settings->value("password").toString());
    }
    settings->endArray();

    QFont currentFont = settings->value("Editor/font").value<QFont>();
    editorSettings->setCurrentFont(currentFont);
}

//------------------------------------------------------------------------------
void Settings::createConnect()
{
    connect(buttonApply, SIGNAL(clicked()),
            editorSettings, SLOT(slotApplySettings()));
    connect(buttonApply, SIGNAL(clicked()),
            connectionSettings, SLOT(slotApplySettings()));
    connect(buttonApply, SIGNAL(clicked()),
            this, SLOT(slotApplySettings()));
    connect(editorSettings, SIGNAL(signalCurrentFont(QFont)),
            this, SIGNAL(signalCurrentFont(QFont)));
    connect(editorSettings, SIGNAL(signalChangeSettings(int)),
            this, SLOT(slotChangeSettings(int)));
    connect(connectionSettings, SIGNAL(signalConnectionData(QList<ConnectData*>)),
            this, SIGNAL(signalConnectionData(QList<ConnectData*>)));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
