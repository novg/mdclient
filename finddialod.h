#ifndef FINDDIALOD_H
#define FINDDIALOD_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QBoxLayout>

class FindDialod : public QDialog
{
    Q_OBJECT
public:
    explicit FindDialod(QWidget *parent = 0);

signals:
    void findNext(const QString &str, Qt::CaseSensitivity cs);
    void findPrev(const QString &str, Qt::CaseSensitivity cs);

private slots:
    void findClicked();
    void enableFindButton(const QString &text);

private:
    void createWidgets();
    void createConnect();

private:
    QLabel      *label;
    QLineEdit   *lineEdit;
    QCheckBox   *caseCheckBox;
    QCheckBox   *backwardCheckBox;
    QPushButton *findButton;
    QPushButton *closeButton;
};

#endif // FINDDIALOD_H
