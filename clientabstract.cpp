#include "clientabstract.h"
#include <QDir>
#include <QMessageBox>

//------------------------------------------------------------------------------
ClientAbstract::ClientAbstract(QWidget *parent) :
    QMainWindow(parent)
{
    mdclient = new TcpClient(this);
}

//------------------------------------------------------------------------------
void ClientAbstract::setConnectData(const ConnectData *connectData)
{
    setObjectName(connectData->objectName());
    address = connectData->getAddress();
    port = connectData->getPort();
    QString userName = connectData->getUserName();
    QString password = connectData->getPassword();
    mdclient->setConnectData(userName, password);
}

//------------------------------------------------------------------------------
void ClientAbstract::runQueueCommands(const QQueue<QString> &queCommand)
{
    mdclient->setQueueCommands(queCommand);
    emit signalProgressBarSetRange(queCommand.size());
}

//------------------------------------------------------------------------------
void ClientAbstract::slotConnected()
{
    emit signalConnected();
}

//------------------------------------------------------------------------------
void ClientAbstract::slotDisconnected()
{
    logfile.close();
    emit signalDisconnected();
}

//------------------------------------------------------------------------------
void ClientAbstract::slotReceiveFromServer(QString &text)
{
    logfile.write(text.toUtf8());
}

//------------------------------------------------------------------------------
void ClientAbstract::openLogFile(QIODevice::OpenMode openMode)
{
    QString fileName = pathLog + "/" + this->objectName() + ".log";
    if (!logfile.isOpen()) {
        logfile.setFileName(fileName);
        if (!logfile.open(openMode | QFile::Text)) {
            QMessageBox::warning(this, tr("Warning"),
                                 tr("Error opening file %1 for writing")
                                 .arg(logfile.fileName()));
            return;
        }
    }
}

//------------------------------------------------------------------------------
void ClientAbstract::createLogDir()
{
    QDir logDir(pathLog);
    if (!logDir.exists()) {
        QDir::current().mkdir(pathLog);
    }
}

//------------------------------------------------------------------------------
void ClientAbstract::createConnect()
{
    connect(mdclient, SIGNAL(signalQueueCommandsEmpty()),
            SLOT(slotQueueCommandsEmpty()));
    connect(mdclient, SIGNAL(signalReceiveFromServer(QString&)),
            this, SLOT(slotReceiveFromServer(QString&)));
    connect(mdclient, SIGNAL(signalProgressBarNextStep()),
            SIGNAL(signalProgressBarNextStep()));
}
