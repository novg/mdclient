#include "editorsettings.h"
#include <QDebug>
#include <QFontComboBox>
#include <QLabel>
#include <QGroupBox>
#include <QBoxLayout>
#include <QComboBox>

//------------------------------------------------------------------------------
EditorSettings::EditorSettings(QWidget *parent) :
    QWidget(parent)
{
    createWidgets();
    createConnect();
}

//------------------------------------------------------------------------------
QFont EditorSettings::getCurrentFont()
{
    return currentFont;
}

//------------------------------------------------------------------------------
void EditorSettings::setCurrentFont(const QFont &font)
{
    comboBoxFont->setCurrentFont(font);
    comboBoxFontSize->setCurrentText(QString("%1").arg(font.pointSize()));
}

//------------------------------------------------------------------------------
void EditorSettings::slotApplySettings()
{
    currentFont = comboBoxFont->currentFont();
    currentFont.setPointSize(comboBoxFontSize->currentText().toInt());
    emit signalCurrentFont(currentFont);
}

//------------------------------------------------------------------------------
void EditorSettings::createWidgets()
{
    QLabel *labelFontName = new QLabel(tr("Name: "));
    QLabel *labelFontSize = new QLabel(tr("\tSize: "));
    comboBoxFont = new QFontComboBox;
    comboBoxFont->setFontFilters(QFontComboBox::FontFilter::MonospacedFonts);
    comboBoxFontSize = new QComboBox;
    addItemsComboBoxFontSize();

    QHBoxLayout *layoutFont = new QHBoxLayout;
    layoutFont->addWidget(labelFontName);
    layoutFont->addWidget(comboBoxFont);
    layoutFont->addWidget(labelFontSize);
    layoutFont->addWidget(comboBoxFontSize);
    layoutFont->addStretch(1);

    QGroupBox *groupBoxFont = new QGroupBox(tr("Font"));
    groupBoxFont->setLayout(layoutFont);
    QVBoxLayout *layoutGroup = new QVBoxLayout(this);
    layoutGroup->addWidget(groupBoxFont);
    layoutGroup->addStretch(1);
}

//------------------------------------------------------------------------------
void EditorSettings::addItemsComboBoxFontSize()
{
    QStringList items;
    items << "6" << "7" << "8" << "9" << "10" << "11"
          << "12" << "14" << "16" << "18" << "20" << "22"
          << "24" << "26" << "28" << "38" << "48" << "72";
    comboBoxFontSize->addItems(items);
}

//------------------------------------------------------------------------------
void EditorSettings::createConnect()
{
    connect(comboBoxFont, SIGNAL(currentIndexChanged(int)),
            this, SIGNAL(signalChangeSettings(int)));
    connect(comboBoxFontSize, SIGNAL(currentIndexChanged(int)),
            this, SIGNAL(signalChangeSettings(int)));
}
