#include "syntaxhighlighter.h"
#include <QTextDocument>
#include <QDebug>

//------------------------------------------------------------------------------
SyntaxHighlighter::SyntaxHighlighter(QTextDocument *parent) :
    QSyntaxHighlighter(parent)
{
}

//------------------------------------------------------------------------------
void SyntaxHighlighter::highlightBlock(const QString &str)
{
    int state = previousBlockState();
    int start = 0;
    int length = 0;
    QString pattern= "^\\w+[:;]";
    QRegExp expression(pattern);

    for (int i = 0; i < str.length(); ++i) {
        if (state == InsideComment) {
            if (str.mid(i, 2) == "*/") {
                state = NormalState;
                setFormat(start, i - start + 2, Qt::darkGray);
                ++i;
            }
        } else if (state == InsideString) {
            if (str.mid(i, 1) == "\"" || str.mid(i, 1) == "\'") {
                if (str.mid(i - 1, 2) != "\\\""
                    && str.mid(i - 1, 2) != "\\\'"
                   ) {
                    state = NormalState;
                    setFormat(start, i - start + 1, Qt::darkGreen);
                }
            }
        } else {
            if (str.mid(i, 2) == "//" || str.mid(i, 1) == "#") {
                setFormat(i, str.length() - i, Qt::darkGray);
                break;
            } else if (str.at(i).isNumber()) {
                setFormat(i, 1, Qt::darkBlue);
            } else if (str.mid(i, 2) == "/*") {
                start = i;
                state = InsideComment;
            } else if (str.mid(i, 1) == "\"" || str.mid(i, 1) == "\'") {
                start = i;
                state = InsideString;
            } else if (str.contains(expression)) {
                start = 0;
                state = InsideCommand;
                length = expression.matchedLength() - 1;
            }
        }
    }

    if (state == InsideComment) {
        setFormat(start, str.length() - start, Qt::darkGray);
    }
    if (state == InsideString) {
        setFormat(start, str.length() - start, Qt::darkGreen);
    }
    if (state == InsideCommand) {
        setFormat(start, length, Qt::darkMagenta);
    }

    setCurrentBlockState(state);
}
