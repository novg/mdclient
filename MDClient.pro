#-------------------------------------------------
#
# Project created by QtCreator 2014-09-15T13:54:30
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MDClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tabbar.cpp \
    texteditor.cpp \
    finddialod.cpp \
    sidebar.cpp \
    settingsbar.cpp \
    settings.cpp \
    editorsettings.cpp \
    connectionname.cpp \
    connectionsettings.cpp \
    widgetclient.cpp \
    client.cpp \
    connectdata.cpp \
    tcpclient.cpp \
    syntaxhighlighter.cpp \
    widgetbase.cpp \
    basetables.cpp \
    basesql.cpp \
    clientbase.cpp \
    progressbar.cpp \
    clientabstract.cpp \
    table.cpp \
    viewsqltable.cpp \
    listitemwidget.cpp

HEADERS  += mainwindow.h \
    tabbar.h \
    texteditor.h \
    finddialod.h \
    sidebar.h \
    settingsbar.h \
    settings.h \
    editorsettings.h \
    connectionname.h \
    connectionsettings.h \
    widgetclient.h \
    client.h \
    connectdata.h \
    tcpclient.h \
    syntaxhighlighter.h \
    widgetbase.h \
    basetables.h \
    basesql.h \
    clientbase.h \
    progressbar.h \
    clientabstract.h \
    table.h \
    viewsqltable.h \
    listitemwidget.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11

RC_FILE = icon.rc

RESOURCES += \
    images.qrc
