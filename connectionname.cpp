#include "connectionname.h"
#include <QDebug>
#include <QLabel>
#include <QLineEdit>
#include <QBoxLayout>
#include <QGroupBox>
#include <QValidator>
#include <QPushButton>

//------------------------------------------------------------------------------
ConnectionName::ConnectionName(const QString &name, QWidget *parent) :
    QWidget(parent)
{
    setObjectName(name);
    createWidgets();

    QValidator *validPort = new QIntValidator(lineEditPort);
    lineEditPort->setValidator(validPort);

    QRegExp rx("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
    QValidator *validIP = new QRegExpValidator(rx, lineEditAddress);
    lineEditAddress->setValidator(validIP);
    lineEditPassword->setEchoMode(QLineEdit::Password);

    connect(buttonDel, SIGNAL(clicked()), this, SLOT(slotDeleteConnect()));
}

//------------------------------------------------------------------------------
QString ConnectionName::getUserName()
{
    return lineEditUserName->text();
}

//------------------------------------------------------------------------------
QString ConnectionName::getPassword()
{
    return lineEditPassword->text();
}

//------------------------------------------------------------------------------
QString ConnectionName::getAddress()
{
    return lineEditAddress->text();
}

//------------------------------------------------------------------------------
QString ConnectionName::getPort()
{
    return lineEditPort->text();
}

//------------------------------------------------------------------------------
void ConnectionName::setUserName(const QString &userName)
{
    lineEditUserName->setText(userName);
}

//------------------------------------------------------------------------------
void ConnectionName::setPassword(const QString &password)
{
    lineEditPassword->setText(password);
}

//------------------------------------------------------------------------------
void ConnectionName::setAddress(const QString &address)
{
    lineEditAddress->setText(address);
}

//------------------------------------------------------------------------------
void ConnectionName::setPort(const QString &port)
{
    lineEditPort->setText(port);
}

//------------------------------------------------------------------------------
void ConnectionName::slotDeleteConnect()
{
    emit signalDeleteConnect(objectName());
//    qDebug() << objectName();
}

//------------------------------------------------------------------------------
void ConnectionName::createWidgets()
{
    QLabel *labelAddress = new QLabel(tr("IP Address:"));
    QLabel *labelPort = new QLabel(tr("Port:"));
    QLabel *labelUserName = new QLabel(tr("User Name:"));
    QLabel *labelPassword = new QLabel(tr("Password:"));
    lineEditAddress = new QLineEdit;
    lineEditPort = new QLineEdit;
    lineEditUserName = new QLineEdit;
    lineEditPassword = new QLineEdit;
    buttonDel = new QPushButton(tr("Delete"));

    QGridLayout *gridLayout = new QGridLayout;
    gridLayout->addWidget(labelAddress, 0, 0);
    gridLayout->addWidget(lineEditAddress, 0, 1);
    gridLayout->addWidget(labelPort, 0, 2);
    gridLayout->addWidget(lineEditPort, 0, 3);
    gridLayout->addWidget(labelUserName, 1, 0);
    gridLayout->addWidget(lineEditUserName, 1, 1);
    gridLayout->addWidget(labelPassword, 1, 2);
    gridLayout->addWidget(lineEditPassword,1, 3);
    gridLayout->addWidget(buttonDel, 1, 4);
    gridLayout->setColumnStretch(5, 1);

    QGroupBox *groupBoxConnect = new QGroupBox(tr("%1").arg(objectName()));
    groupBoxConnect->setLayout(gridLayout);

    QVBoxLayout *layoutName = new QVBoxLayout(this);
    layoutName->addWidget(groupBoxConnect);
    layoutName->addStretch(1);
}
