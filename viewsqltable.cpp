#include "viewsqltable.h"
#include "listitemwidget.h"
#include <QSqlTableModel>
#include <QTableView>
#include <QBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QStackedWidget>
#include <QSqlQuery>
#include <QPushButton>
#include <QMap>
#include <QPair>
#include <QShortcut>
#include <QItemSelectionModel>
#include <QClipboard>
#include <QApplication>
#include <QDebug>

//------------------------------------------------------------------------------
ViewSqlTable::ViewSqlTable(QSqlDatabase *dbase, QWidget *parent) :
    QWidget(parent)
{
    tableCross    = "cross";
    tableComments = "comments";
    tableNumplan  = "numplan";
    tableDirnum   = "dirnum";
    tableAbbnum   = "abbnum";
    tableNamenum  = "namenum";
    listLoadTables << tableCross << tableNumplan;

    db = dbase;
    createWidgets();
    updateListTables();

    connect(comboTables, SIGNAL(activated(QString)),
            SLOT(slotShowTable(QString)));
    connect(copyToClipboard, SIGNAL(activated()),
            SLOT(slotCopyToClipboard()));
}

//------------------------------------------------------------------------------
void ViewSqlTable::slotShowTable(const QString &table)
{
    if (view->model()) {
        QAbstractItemModel *oldModel = view->model();
        delete oldModel;
        oldModel = nullptr;
    }

    model = new QSqlTableModel(this, *db);
    model->setTable(table);
    model->select();
    view->setModel(model);

    selection = new QItemSelectionModel(model);
    view->setSelectionModel(selection);

    QStringList listColumns;
    for (int i = 0; i < model->columnCount(); ++i)
        listColumns << model->headerData(i, Qt::Horizontal).toString();

    createStackWidget(table, listColumns);

    setLabelRows(QString("SELECT COUNT(*) FROM %1").arg(table));
}

//------------------------------------------------------------------------------
void ViewSqlTable::slotFilterTable()
{
    QList<ListItemWidget *> widgets =
            stackWidget->currentWidget()->findChildren<ListItemWidget *>();

    QMap<QString, QString> mapRequests;
    for (auto listItem : widgets) {
        QPair<QString, QString> pair = listItem->getRequest();
        mapRequests.insert(pair.first, pair.second);
    }

    QString strFilter;
    QMapIterator<QString, QString> i(mapRequests);
    while (i.hasNext()) {
        i.next();
        if (!i.value().isEmpty()) {
            strFilter += filterColumn(i.key(), i.value());
            strFilter += " AND ";
        }
    }

    if (!strFilter.isEmpty()) {
        strFilter.chop(5);
        strFilter += ";";
        model->setFilter(strFilter);

        QString strCountRows = QString("SELECT COUNT(*) FROM %1 WHERE ")
                .arg(currentTable);
        strCountRows += strFilter;
        setLabelRows(strCountRows);
    }
}

//------------------------------------------------------------------------------
void ViewSqlTable::slotClearFields()
{
    QList<ListItemWidget *> widgets =
            stackWidget->currentWidget()->findChildren<ListItemWidget *>();

    for (auto listItem : widgets)
        listItem->clearField();

    model->setFilter("");
    setLabelRows(QString("SELECT COUNT(*) FROM %1").arg(currentTable));
}

//------------------------------------------------------------------------------
void ViewSqlTable::slotLoadFile()
{
    emit signalLoadFile(currentTable);
}

//------------------------------------------------------------------------------
void ViewSqlTable::slotCopyToClipboard()
{
    QClipboard *clipboard = QApplication::clipboard();
    QModelIndexList listIndex = selection->selectedIndexes();
    QString strClipboard;
    int currentRow = listIndex.at(0).row();
    for (QModelIndex index : listIndex) {
        if (currentRow == index.row()) {
            strClipboard += "\t";
        } else {
            strClipboard += "\n";
            currentRow = index.row();
        }
        strClipboard += model->data(index).toString();
    }

    strClipboard.remove(0,1);
    clipboard->setText(strClipboard);
}

//------------------------------------------------------------------------------
void ViewSqlTable::createWidgets()
{
    QLabel *labelSelectTable = new QLabel(tr("Select the table:"));
    comboTables = new QComboBox;
    stackWidget = new QStackedWidget;
    labelRows = new QLabel(tr("Rows: 0"));
    view = new QTableView;

    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addWidget(labelSelectTable);
    hLayout->addWidget(comboTables);
    hLayout->addWidget(labelRows);
    hLayout->addStretch(1);

    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->addLayout(hLayout);
    vLayout->addWidget(stackWidget);
    vLayout->addWidget(view, 1);
    setLayout(vLayout);

    copyToClipboard = new QShortcut(QKeySequence("Ctrl+C"),this);
}

//------------------------------------------------------------------------------
void ViewSqlTable::updateListTables()
{
    comboTables->clear();
    comboTables->addItems(db->tables() + db->tables(QSql::Views));
    comboTables->setSizeAdjustPolicy(QComboBox::AdjustToContents);
}

//------------------------------------------------------------------------------
void ViewSqlTable::createStackWidget(const QString &table, const QStringList &listColumns)
{
    currentTable = table;
    static QStringList tables;
    if (!tables.contains(table)) {
        tables << table;
        QHBoxLayout *hLayout = new QHBoxLayout;
        for (auto column : listColumns) {
            ListItemWidget *listItemWidget = new ListItemWidget(column);
            hLayout->addWidget(listItemWidget);
            connect(listItemWidget, SIGNAL(signalEditingFinished()),
                    SLOT(slotFilterTable()));
        }
        hLayout->addStretch(1);

        QPushButton *pushClear = new QPushButton(tr("Clear fields:"));
        connect(pushClear, SIGNAL(clicked()), SLOT(slotClearFields()));

        QPushButton *pushLoadFile = new QPushButton(tr("Load file"));
        connect(pushLoadFile, SIGNAL(clicked()), SLOT(slotLoadFile()));

        if (!listLoadTables.contains(table))
            pushLoadFile->hide();

        QHBoxLayout *pushLayout = new QHBoxLayout;
        pushLayout->addWidget(pushClear);
        pushLayout->addStretch(1);
        pushLayout->addWidget(pushLoadFile);

        QVBoxLayout *vLayout = new QVBoxLayout;
        vLayout->addLayout(pushLayout);
        vLayout->addLayout(hLayout);

        QWidget *widget = new QWidget;
        widget->setLayout(vLayout);
        stackWidget->addWidget(widget);
    }

    stackWidget->setCurrentIndex(tables.indexOf(table));
}

//------------------------------------------------------------------------------
void ViewSqlTable::setLabelRows(const QString &request)
{
    QSqlQuery query(request);
    query.first();
    labelRows->setText(tr("Rows: %1").arg(query.value(0).toInt()));
}

//------------------------------------------------------------------------------
QString ViewSqlTable::filterColumn(const QString &column, const QString &request) const
{
    QString strRequest;
    QString strFilter;

    if (request.contains(",")) {
        QStringList tempList = request.split(",");
        for (auto &str : tempList) {
            str = "'" + str + "',";
            strRequest += str;
        }
        strRequest.chop(1);
        strFilter = "%1 IN (%2)";
    } else {
        strRequest = request;
        QRegExp rx("^\\d+$");
        if (rx.exactMatch(strRequest)) {
            strFilter = "%1=%2";
        } else {
            strFilter = "%1 LIKE ('%%2%')";
        }
    }

    strFilter = strFilter.arg(column).arg(strRequest);

    return strFilter;
}
