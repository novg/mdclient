#ifndef TABBAR_H
#define TABBAR_H

#include <QTabBar>
#include <QStylePainter>
#include <QStyleOptionTabV3>
#include <QPainter>
#include <QIcon>
#include <QString>

class TabBar : public QTabBar
{
    Q_OBJECT
public:
    explicit TabBar(QWidget *parent = 0);

protected:
    QSize tabSizeHint(int) const;
    void paintEvent(QPaintEvent *);

signals:

public slots:

};

#endif // TABBAR_H
