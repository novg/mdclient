#ifndef WIDGETCLIENT_H
#define WIDGETCLIENT_H

#include "texteditor.h"
#include "client.h"
#include "connectdata.h"
#include <QMainWindow>

class QTabWidget;

class WidgetClient : public QMainWindow
{
    Q_OBJECT
public:
    explicit WidgetClient(QWidget *parent = 0);

public:
    void closeProgram();

signals:
    void signalTextChanged(const QPair<QString, bool> &documentChange);
    void signalCloseProgram();
    void signalConnected();
    void signalDisconnected();
    void signalConnectionEnable();
    void signalConnectionDisable();
    void signalProgressBarReset();
    void signalProgressBarSetRange(const int &num);
    void signalProgressBarNextStep();

public slots:
    void slotSetFont(const QFont &currentFont);
    void slotSetConnections(QList<ConnectData*> listConnections);
    void slotConnected();
    void slotDisconnected();

private slots:
    void slotSetTabTitle(const QString &name);
    void slotQueueCommands();
    void slotAddTab();
    void slotDelTab(int index);
    void slotDelTab();

private:
    void deleteTab(int index);
    void createWidgets();

private:
    QTabWidget *tabEditor;
    QTabWidget *tabConnect;
    QFont font;
};

#endif // WIDGETCLIENT_H
