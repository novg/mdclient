#ifndef BASETABLES_H
#define BASETABLES_H

#include "table.h"
#include "viewsqltable.h"
#include <QWidget>
#include <QMap>
#include <QSqlQuery>

class QSqlDatabase;

class BaseTables : public QWidget
{
    Q_OBJECT
public:
    explicit BaseTables(QWidget *parent = 0);
    ~BaseTables();

public:
    void setCountConnections(const int count);
    void setAllow();

signals:

public slots:
    void slotReceiveLogName(const QString &fileName);

private slots:
    void slotFillTables(const QString &table);

private:
    void createWidgets();
    bool createTables();
    bool initTables(const QString &name, const QString &create, const QString &insert);
    bool createDataBaseConnect();
    void baseUpdate();
    bool fillTables(const QString &fileName);
    bool clearTables();

private:
    QSqlDatabase *db;
    int countConnections;
    QString dbname;
    QString tableCross;
    QString tableComments;
    QString tableNumplan;
    QString tableDirnum;
    QString tableAbbnum;
    QString tableNamenum;
    QString viewTotal;
    QString viewFreeEquMd110;
    QString viewFreeEquAastra;
    QString viewFreeNumbers;
    QStringList logFiles;
    QString path;
    QMap<QString, Table*> mapTables;
    QSqlQuery query;
    bool dbConnect;
    ViewSqlTable *viewSqlTable;
    QStringList clearList;
    bool allow;
};

#endif // BASETABLES_H
