#include "tabbar.h"

TabBar::TabBar(QWidget *parent) :
    QTabBar(parent)
{
    setIconSize(QSize(64, 64));
}

QSize TabBar::tabSizeHint(int) const
{
    return QSize(90, 90);
}

void TabBar::paintEvent(QPaintEvent *)
{
    QStylePainter p(this);
    for (int index = 0; index < count(); ++index) {
        QStyleOptionTabV3 tab;
        initStyleOption(&tab, index);

        QIcon tempIcon = tab.icon;
        QString tempText = tab.text;
        tab.icon = QIcon();
        tab.text = QString();

        p.drawControl(QStyle::CE_TabBarTab, tab);

        QPainter painter;
        painter.begin(this);
        QRect tabrect = tabRect(index);
        tabrect.adjust(0, 5, 0, -5);
        painter.drawText(tabrect, Qt::AlignBottom | Qt::AlignHCenter, tempText);
        int x = (tabrect.size().width() - tab.iconSize.width()) / 2;
        tempIcon.paint(&painter, x, tabrect.top(), tab.iconSize.width(),
                       tab.iconSize.height(), Qt::AlignTop | Qt::AlignHCenter);
        painter.end();
    }
}
