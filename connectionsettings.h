#ifndef CONNECTIONSETTINGS_H
#define CONNECTIONSETTINGS_H

#include "connectionname.h"
#include "connectdata.h"
#include <QWidget>
class QPushButton;
class QStackedWidget;
class QGridLayout;

class ConnectionSettings : public QWidget
{
    Q_OBJECT
public:
    explicit ConnectionSettings(QWidget *parent = 0);

public:
    QList<ConnectionName *> listConnections;

signals:
    void signalTransmitConnectionName(const QString &name);
    void signalConnectionData(QList<ConnectData *>);

public slots:
    void slotAddConnection(const QString &name);
    void slotApplySettings();

private slots:
    void slotDialogEnterName();
    void slotDeleteConnection(const QString &name);

private:
    void createWidgets();
    void createConnect();

private:
    int buttonWidth;
    ConnectionName *connectionName;
    QPushButton *buttonAddConnect;
    QGridLayout *layoutAddConnect;
    QStackedWidget *stacketWidget;
};

#endif // CONNECTIONSETTINGS_H
