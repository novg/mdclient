#include "client.h"
#include <QPlainTextEdit>
#include <QAction>
#include <QToolBar>

//------------------------------------------------------------------------------
Client::Client(QWidget *parent) :
    ClientAbstract(parent)
{
    connected = false;
    queueCommandsEmpty = true;
    pathLog = "log";
    createLogDir();
    createWidgets();
    createActions();
    createConnect();
}

//------------------------------------------------------------------------------
void Client::slotConnectMode()
{
    if (connected) {
        mdclient->slotDisconnectFromHost();
        emit signalProgressBarReset();
    } else {
        mdclient->connectToHost(address, port);
        openLogFile(QFile::Append);
    }
}

//------------------------------------------------------------------------------
void Client::slotConnected()
{
    ClientAbstract::slotConnected();
    actionConnect->setIcon(QIcon(":/images/connect_no.png"));
    connected = true;
}

//------------------------------------------------------------------------------
void Client::slotDisconnected()
{
    ClientAbstract::slotDisconnected();
    actionConnect->setIcon(QIcon(":/images/connect.png"));
    actionRunCommand->setEnabled(false);
    connected = false;
}

//------------------------------------------------------------------------------
void Client::slotReceiveFromServer(QString &text)
{
    ClientAbstract::slotReceiveFromServer(text);
    actionRunCommand->setEnabled(true);
    textOutput->moveCursor(QTextCursor::End);
    textOutput->insertPlainText(text);
}

//------------------------------------------------------------------------------
void Client::slotQueueCommandsEmpty()
{
    queueCommandsEmpty = true;
    actionRunCommand->setToolTip(tr("RunCommand"));
    actionRunCommand->setStatusTip(tr("RunCommand"));
    actionRunCommand->setWhatsThis(tr("RunCommand"));
    actionRunCommand->setIcon(QIcon(":/images/play.png"));
    emit signalProgressBarReset();
}

//------------------------------------------------------------------------------
void Client::slotRequireCommads()
{
    if (queueCommandsEmpty) {
        emit signalRequireCommands();
    } else {
        mdclient->emptyQueue();
    }
}

//------------------------------------------------------------------------------
void Client::runQueueCommands(const QQueue<QString> &queCommand)
{
    ClientAbstract::runQueueCommands(queCommand);
    queueCommandsEmpty = false;
    actionRunCommand->setToolTip(tr("StopCommand"));
    actionRunCommand->setStatusTip(tr("StopCommand"));
    actionRunCommand->setWhatsThis(tr("StopCommand"));
    actionRunCommand->setIcon(QIcon(":/images/stop.png"));
}

//------------------------------------------------------------------------------
void Client::setCurrentFont(const QFont &currentFont)
{
    textOutput->setFont(currentFont);
}

//------------------------------------------------------------------------------
void Client::slotConnectionEnable()
{
    actionConnect->setEnabled(true);
}

//------------------------------------------------------------------------------
void Client::slotConnectionDisable()
{
    actionConnect->setEnabled(false);
}

//------------------------------------------------------------------------------
void Client::createWidgets()
{
    textOutput = new QPlainTextEdit;
    textOutput->setReadOnly(true);
    setCentralWidget(textOutput);
}

//------------------------------------------------------------------------------
void Client::createActions()
{
    actionConnect = new QAction(tr("Connect"), this);
    actionConnect->setToolTip(tr("Connect to ATS"));
    actionConnect->setStatusTip(tr("Connect to ATS"));
    actionConnect->setWhatsThis(tr("Connect to ATS"));
    actionConnect->setIcon(QIcon(":/images/connect.png"));

    actionRunCommand = new QAction(tr("RunCommand"), this);
    actionRunCommand->setToolTip(tr("RunCommand"));
    actionRunCommand->setStatusTip(tr("RunCommand"));
    actionRunCommand->setWhatsThis(tr("RunCommand"));
    actionRunCommand->setIcon(QIcon(":/images/play.png"));
    actionRunCommand->setEnabled(false);

    QToolBar *toolBar = addToolBar(tr("Client toolBar"));
    toolBar->addAction(actionConnect);
    toolBar->addSeparator();
    toolBar->addAction(actionRunCommand);
}

//------------------------------------------------------------------------------
void Client::createConnect()
{
    ClientAbstract::createConnect();
    connect(mdclient, SIGNAL(signalConnected()),
            this, SLOT(slotConnected()));
    connect(mdclient, SIGNAL(signalDisconnected()),
            this, SLOT(slotDisconnected()));
    connect(actionConnect, SIGNAL(triggered()),
            this, SLOT(slotConnectMode()));
    connect(actionRunCommand, SIGNAL(triggered()),
            this, SLOT(slotRequireCommads()));
}
