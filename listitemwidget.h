#ifndef LISTITEMWIDGET_H
#define LISTITEMWIDGET_H

#include <QWidget>
#include <QPair>

class QLineEdit;

class ListItemWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ListItemWidget(const QString &name, QWidget *parent = 0);

public:
    void clearField();
    QPair<QString, QString> getRequest() const;

signals:
//    void signalEditingFinished(const QString &column, const QString &request);
    void signalEditingFinished();

//private slots:
//    void slotEditingFinished();

private:
    QString column;
    QLineEdit *lineEdit;
};

#endif // LISTITEMWIDGET_H
