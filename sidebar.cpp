#include "sidebar.h"
#include "tabbar.h"

Sidebar::Sidebar(QWidget *parent) :
    QTabWidget(parent)
{
    setTabBar(new TabBar());
}
