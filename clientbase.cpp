#include "clientbase.h"
#include <QMessageBox>

//------------------------------------------------------------------------------
ClientBase::ClientBase(QWidget *parent) :
    ClientAbstract(parent)
{
    pathLog = "dbase";
    createLogDir();
    createConnect();
}

//------------------------------------------------------------------------------
void ClientBase::connectToHost()
{
    mdclient->connectToHost(address, port);
    openLogFile(QFile::WriteOnly);
}

//------------------------------------------------------------------------------
void ClientBase::slotQueueCommandsEmpty()
{
    mdclient->slotDisconnectFromHost();
    logfile.close();
    emit signalProgressBarReset();
    emit signalLogName(logfile.fileName());
}

//------------------------------------------------------------------------------
void ClientBase::createConnect()
{
    ClientAbstract::createConnect();
    connect(mdclient, SIGNAL(signalDisconnected()),
            SIGNAL(signalDisconnected()));
    connect(mdclient, SIGNAL(signalConnected()),
            SIGNAL(signalConnected()));
}
