#include "finddialod.h"

//------------------------------------------------------------------------------
FindDialod::FindDialod(QWidget *parent) :
    QDialog(parent)
{
    createWidgets();
    createConnect();

    setWindowTitle(tr("Find"));
    setWindowIcon(QIcon("://images/search.png"));
    setFixedHeight(sizeHint().height());
}

//------------------------------------------------------------------------------
void FindDialod::findClicked()
{
    QString text = lineEdit->text();
    Qt::CaseSensitivity cs =
            caseCheckBox->isChecked() ? Qt::CaseSensitive
                                      : Qt::CaseInsensitive;
    if (backwardCheckBox->isChecked()) {
        emit findPrev(text, cs);
    } else {
        emit findNext(text, cs);
    }
}

//------------------------------------------------------------------------------
void FindDialod::enableFindButton(const QString &text)
{
    findButton->setEnabled(!text.isEmpty());
}

//------------------------------------------------------------------------------
void FindDialod::createWidgets()
{
    label = new QLabel(tr("Find &what:"));
    lineEdit = new QLineEdit;
    label->setBuddy(lineEdit);

    caseCheckBox = new QCheckBox(tr("Match &case"));
    backwardCheckBox = new QCheckBox(tr("Search backward"));

    findButton = new QPushButton(tr("&Find"));
    findButton->setDefault(true);
    findButton->setEnabled(false);

    closeButton = new QPushButton(tr("Close"));

    QHBoxLayout *topLeftLayout = new QHBoxLayout;
    topLeftLayout->addWidget(label);
    topLeftLayout->addWidget(lineEdit);

    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addLayout(topLeftLayout);
    leftLayout->addWidget(caseCheckBox);
    leftLayout->addWidget(backwardCheckBox);

    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(findButton);
    rightLayout->addWidget(closeButton);
    rightLayout->addStretch();

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);

    setLayout(mainLayout);
}

//------------------------------------------------------------------------------
void FindDialod::createConnect()
{
    connect(lineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(enableFindButton(QString)));
    connect(findButton, SIGNAL(clicked()),
            this, SLOT(findClicked()));
    connect(closeButton, SIGNAL(clicked()),
            this, SLOT(close()));
}
