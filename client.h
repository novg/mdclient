#ifndef CLIENT_H
#define CLIENT_H

#include "clientabstract.h"

class QPlainTextEdit;

class Client : public ClientAbstract
{
    Q_OBJECT
public:
    explicit Client(QWidget *parent = 0);

public:
    void runQueueCommands(const QQueue<QString> &queCommand);
    void setCurrentFont(const QFont &currentFont);

signals:

public slots:
    void slotConnectionEnable();
    void slotConnectionDisable();

private slots:
    void slotConnectMode();
    void slotConnected();
    void slotDisconnected();
    void slotReceiveFromServer(QString &text);
    void slotQueueCommandsEmpty();
    void slotRequireCommads();

private:
    void createWidgets();
    void createActions();
    void createConnect();

private:
    QPlainTextEdit *textOutput;
    QAction *actionConnect;
    QAction *actionRunCommand;
    bool connected;
    bool queueCommandsEmpty;
};

#endif // CLIENT_H
