#include "widgetbase.h"
#include "clientbase.h"
#include <QAction>
#include <QToolBar>
#include <QQueue>
#include <QShortcut>

//------------------------------------------------------------------------------
WidgetBase::WidgetBase(QWidget *parent) :
    QMainWindow(parent)
{
    clientCount = 0;
    barCount = 0;
    rangeCount = 0;
    range = 0;
    createWidgets();
    createActions();
    connect(actionBaseUpdate, SIGNAL(triggered()), SLOT(slotQueueCommands()));
    connect(shortcutAllow, SIGNAL(activated()), SLOT(slotAllow()));
}

//------------------------------------------------------------------------------
void WidgetBase::slotSetConnections(QList<ConnectData *> listConnections)
{
    baseTables->setCountConnections(listConnections.size());
    for (int i = 0; i < listConnections.size(); ++i) {
        ClientBase *client = new ClientBase;
        client->setConnectData(listConnections.at(i));
        listClient << client;
        connect(client, SIGNAL(signalDisconnected()),
                SLOT(slotDisconnected()));
        connect(client, SIGNAL(signalConnected()),
                SLOT(slotConnected()));
        connect(client, SIGNAL(signalProgressBarReset()),
                SLOT(slotProgressBarReset()));
        connect(client, SIGNAL(signalProgressBarSetRange(int)),
                SLOT(slotProgressBarSetRange(int)));
        connect(client, SIGNAL(signalProgressBarNextStep()),
                SIGNAL(signalProgressBarNextStep()));
        connect(client, SIGNAL(signalLogName(QString)),
                baseTables, SLOT(slotReceiveLogName(QString)));
    }
}

//------------------------------------------------------------------------------
void WidgetBase::slotQueueCommands()
{
    //TODO: uncomment
    QQueue<QString> queCommand;
    queCommand.enqueue("CADAP;\n");
    queCommand.enqueue("EXDDP:DIR=ALL;\n");
    queCommand.enqueue("KSDDP:DIR=ALL;\n");
    queCommand.enqueue("GEDIP:DIR=ALL;\n");
    queCommand.enqueue("ADCDP:ABB=ALL;\n");
    queCommand.enqueue("NIINP:DIR=ALL;\n");

    for (int i = 0; i < listClient.size(); ++i) {
        ClientBase *client = qobject_cast<ClientBase *>(listClient.at(i));
        if (client) {
            client->connectToHost();
            client->runQueueCommands(queCommand);
        }
    }
    //TODO: delete
//    baseTables->slotReceiveLogName("dbase/md110.log");
//    baseTables->slotReceiveLogName("dbase/aastra.log");
}

//------------------------------------------------------------------------------
void WidgetBase::slotDisconnected()
{
    ++clientCount;
    if (clientCount == listClient.size()) {
        actionBaseUpdate->setEnabled(true);
        clientCount = 0;
        emit signalDisconnected();
    }
}

//------------------------------------------------------------------------------
void WidgetBase::slotDisconnect()
{
    actionBaseUpdate->setEnabled(true);
}

//------------------------------------------------------------------------------
void WidgetBase::slotConnected()
{
    actionBaseUpdate->setEnabled(false);
    emit signalConnected();
}

//------------------------------------------------------------------------------
void WidgetBase::slotConnect()
{
    actionBaseUpdate->setEnabled(false);
}

//------------------------------------------------------------------------------
void WidgetBase::slotProgressBarReset()
{
    ++barCount;
    if (barCount == listClient.size()) {
        emit signalProgressBarReset();
        barCount = 0;
    }
}

//------------------------------------------------------------------------------
void WidgetBase::slotProgressBarSetRange(const int &num)
{
    range+= num;
    ++rangeCount;
    if (rangeCount == listClient.size()) {
        emit signalProgressBarSetRange(range);
        range = 0;
        rangeCount = 0;
    }
}

//------------------------------------------------------------------------------
void WidgetBase::slotAllow()
{
   actionBaseUpdate->setIcon(QIcon(":/images/db_comit.png"));
   baseTables->setAllow();
}

//------------------------------------------------------------------------------
void WidgetBase::createWidgets()
{
    tabBase = new QTabWidget(this);
    baseTables = new BaseTables;
    baseSql = new BaseSql;

    tabBase->addTab(baseTables, "BaseTables");
    tabBase->addTab(baseSql, "BaseSql");

    setCentralWidget(tabBase);
}

//------------------------------------------------------------------------------
void WidgetBase::createActions()
{
   actionBaseUpdate  = new QAction(tr("Base Update"), this);
   actionBaseUpdate->setToolTip(tr("Base update"));
   actionBaseUpdate->setStatusTip(tr("Base update"));
   actionBaseUpdate->setWhatsThis(tr("Base update"));
   actionBaseUpdate->setIcon(QIcon(":/images/db_update.png"));

   shortcutAllow = new QShortcut(QKeySequence("Ctrl+Alt+F10"), this);

//   actionAllow = new QAction(tr(""))

   QToolBar *toolBar = addToolBar("Base update");
   toolBar->addAction(actionBaseUpdate);
}
