#ifndef WIDGETBASE_H
#define WIDGETBASE_H

#include "basetables.h"
#include "basesql.h"
#include "connectdata.h"
#include <QMainWindow>

class WidgetBase : public QMainWindow
{
    Q_OBJECT
public:
    explicit WidgetBase(QWidget *parent = 0);

signals:
    void signalConnected();
    void signalDisconnected();
    void signalProgressBarReset();
    void signalProgressBarSetRange(const int &num);
    void signalProgressBarNextStep();

public slots:
    void slotSetConnections(QList<ConnectData*> listConnections);

private slots:
    void slotQueueCommands();
    void slotDisconnected();
    void slotDisconnect();
    void slotConnected();
    void slotConnect();
    void slotProgressBarReset();
    void slotProgressBarSetRange(const int &num);
    void slotAllow();

private:
    void createWidgets();
    void createActions();

private:
    QTabWidget *tabBase;
    BaseTables *baseTables;
    BaseSql    *baseSql;
    QAction *actionBaseUpdate;
    QShortcut *shortcutAllow;
    QList<QWidget *> listClient;
    int clientCount;
    int barCount;
    int rangeCount;
    int range;
};

#endif // WIDGETBASE_H
