#ifndef CLIENTBASE_H
#define CLIENTBASE_H

#include "clientabstract.h"

class ClientBase : public ClientAbstract
{
    Q_OBJECT
public:
    explicit ClientBase(QWidget *parent = 0);

public:
    void connectToHost();

signals:
    void signalDisconnected();
    void signalConnected();
    void signalLogName(const QString &name);

public slots:

private slots:
    void slotQueueCommandsEmpty();

private:
    void createConnect();

};

#endif // CLIENTBASE_H
