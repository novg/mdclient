#include "progressbar.h"
#include <QProgressBar>
#include <QVBoxLayout>

//------------------------------------------------------------------------------
ProgressBar::ProgressBar(QWidget *parent) :
    QWidget(parent)
{
    step = 0;
    range = 0;
    createWidgets();
}

//------------------------------------------------------------------------------
void ProgressBar::slotInit(const int &num)
{
    step = 0;
    range = num;
    progressBar->reset();
    progressBar->setRange(0, range);
    progressBar->show();
}

//------------------------------------------------------------------------------
void ProgressBar::slotNextStep()
{
    progressBar->setValue(++step);
}

//------------------------------------------------------------------------------
void ProgressBar::slotReset()
{
    progressBar->hide();
}

//------------------------------------------------------------------------------
void ProgressBar::createWidgets()
{
    progressBar = new QProgressBar;
//    progressBar->setTextVisible(false);
    progressBar->hide();

    QVBoxLayout *vboxLayout = new QVBoxLayout;
    vboxLayout->addWidget(progressBar);
    setLayout(vboxLayout);
}
