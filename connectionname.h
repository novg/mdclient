#ifndef CONNECTIONNAME_H
#define CONNECTIONNAME_H

#include <QWidget>
class QLineEdit;
class QPushButton;

class ConnectionName : public QWidget
{
    Q_OBJECT
public:
    explicit ConnectionName(const QString &name, QWidget *parent = 0);

public:
    QString getUserName();
    QString getPassword();
    QString getAddress();
    QString getPort();
    void setUserName(const QString &userName);
    void setPassword(const QString &password);
    void setAddress(const QString &address);
    void setPort(const QString &port);

signals:
    void signalDeleteConnect(const QString &connectName);

public slots:

private slots:
    void slotDeleteConnect();

private:
    void createWidgets();

private:
    QLineEdit *lineEditUserName;
    QLineEdit *lineEditPassword;
    QLineEdit *lineEditAddress;
    QLineEdit *lineEditPort;
    QPushButton *buttonDel;
};

#endif // CONNECTIONNAME_H
