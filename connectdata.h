#ifndef CONNECTDATA_H
#define CONNECTDATA_H

#include <QObject>

class ConnectData : public QObject
{
    Q_OBJECT
public:
    explicit ConnectData(QObject *parent = 0);

public:
    void setAddress(const QString &addr);
    void setPort(const QString &prt);
    void setUserName(const QString &name);
    void setPassword(const QString &passw);
    QString getAddress() const;
    QString getPort() const;
    QString getUserName() const;
    QString getPassword() const;

signals:

public slots:

private:
    QString address;
    QString port;
    QString userName;
    QString password;
};

#endif // CONNECTDATA_H
