#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "sidebar.h"
#include "widgetclient.h"
#include "settings.h"
#include "widgetbase.h"
#include "progressbar.h"
#include <QMainWindow>

//namespace Ui {
//class MainWindow;
//}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void slotTextChanged(const QPair<QString, bool> &documentChange);

private:
    void createConnect();
    void readSettings();
    bool maybeSave();
    bool textChanged();
    void createWidgets();

private:
    WidgetClient *widgetClient;
    Settings *settings;
    WidgetBase *widgetBase;
    QMap<QString, bool> documentsChange;
    ProgressBar *progressBar;
};

#endif // MAINWINDOW_H
