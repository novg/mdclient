#ifndef BASESQL_H
#define BASESQL_H

#include <QWidget>
#include <QTableView>

class QLabel;
class QLineEdit;
class QPushButton;
class QTextEdit;

class BaseSql : public QWidget
{
    Q_OBJECT
public:
    explicit BaseSql(QWidget *parent = 0);

signals:

public slots:

private slots:
    void slotShowTable();

private:
    void createWidgets();

private:
    QTableView *view;
    QLabel *labelRows;
    QLineEdit *errorEdit;
    QPushButton *pushSql;
    QTextEdit *tSqlQuery;
};

#endif // BASESQL_H
