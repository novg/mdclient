#include "listitemwidget.h"
#include <QLineEdit>
#include <QLabel>
#include <QBoxLayout>

//------------------------------------------------------------------------------
ListItemWidget::ListItemWidget(const QString &name, QWidget *parent) :
    QWidget(parent)
{
    column = name;
    lineEdit = new QLineEdit;
    QLabel *label = new QLabel(column);
    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->addWidget(label);
    vLayout->addWidget(lineEdit);
    vLayout->setMargin(1);
    setLayout(vLayout);

//    connect(lineEdit, SIGNAL(returnPressed()), SLOT(slotEditingFinished()));
    connect(lineEdit, SIGNAL(returnPressed()), SIGNAL(signalEditingFinished()));
}

void ListItemWidget::clearField()
{
    lineEdit->clear();
}

//------------------------------------------------------------------------------
QPair<QString, QString> ListItemWidget::getRequest() const
{
    QPair<QString, QString> request;

    if (!lineEdit->text().isEmpty()) {
        request.first = column;
        request.second = lineEdit->text();
    }

    return request;
}

//------------------------------------------------------------------------------
//void ListItemWidget::slotEditingFinished()
//{
//    emit signalEditingFinished(column, lineEdit->text());
//}
