#ifndef SYNTAXHIGHLIGHTER_H
#define SYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>

class QTextDocument;

class SyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    explicit SyntaxHighlighter(QTextDocument *parent = 0);

signals:

public slots:

private:
    enum State {NormalState = -1, InsideComment, InsideString, InsideCommand};
    virtual void highlightBlock(const QString &str);
};

#endif // SYNTAXHIGHLIGHTER_H
