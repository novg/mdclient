#include "mainwindow.h"
#include <QTabWidget>
#include <QLabel>
#include <QBoxLayout>
#include <QMessageBox>
#include <QStatusBar>

//------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    createWidgets();
    createConnect();
    readSettings();
}

//------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
}

//------------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

//------------------------------------------------------------------------------
void MainWindow::slotTextChanged(const QPair<QString, bool> &documentChange)
{
    documentsChange.insert(documentChange.first, documentChange.second);
}

//------------------------------------------------------------------------------
void MainWindow::readSettings()
{
    settings->getSettings();
}

//------------------------------------------------------------------------------
bool MainWindow::maybeSave()
{
    if (textChanged()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("MDClient"),
                                   tr("The document has been modified.\n"
                                      "Do you want to save your changes ?"),
                                   QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel
                                   );
        if (ret == QMessageBox::Save) {
            widgetClient->closeProgram();
            return true;
        } else if (ret == QMessageBox::Cancel) {
            return false;
        }
    }

    return true;
}

//------------------------------------------------------------------------------
bool MainWindow::textChanged()
{
    bool change = false;
    for (auto value : documentsChange) {
        if (value) {
            change = true;
            break;
        }
    }

    return change;
}

//------------------------------------------------------------------------------
void MainWindow::createWidgets()
{
    Sidebar *sidebar = new Sidebar;
    sidebar->setTabPosition(Sidebar::West);

    widgetClient = new WidgetClient;
    widgetBase = new WidgetBase;
    settings = new Settings;

    sidebar->addTab(widgetClient, QIcon(":/images/utilities-terminal.png"), "Client");
    sidebar->addTab(widgetBase, QIcon(":/images/database_blue.png"), "Base");
    sidebar->addTab(settings, QIcon(":/images/advancedsettings_blue.png"), "Settings");

    progressBar = new ProgressBar(this);
    statusBar()->addPermanentWidget(progressBar);

    setCentralWidget(sidebar);
}

//------------------------------------------------------------------------------
void MainWindow::createConnect()
{
    connect(settings, SIGNAL(signalCurrentFont(QFont)),
            widgetClient, SLOT(slotSetFont(QFont)));
    connect(settings, SIGNAL(signalConnectionData(QList<ConnectData*>)),
            widgetClient, SLOT(slotSetConnections(QList<ConnectData*>)));
    connect(settings, SIGNAL(signalConnectionData(QList<ConnectData*>)),
            widgetBase, SLOT(slotSetConnections(QList<ConnectData*>)));
    connect(widgetClient, SIGNAL(signalTextChanged(QPair<QString,bool>)),
            SLOT(slotTextChanged(QPair<QString,bool>)));
    connect(widgetClient, SIGNAL(signalConnected()),
            widgetBase, SLOT(slotConnect()));
    connect(widgetClient, SIGNAL(signalDisconnected()),
            widgetBase, SLOT(slotDisconnect()));
    connect(widgetBase, SIGNAL(signalConnected()),
            widgetClient, SLOT(slotConnected()));
    connect(widgetBase, SIGNAL(signalDisconnected()),
            widgetClient, SLOT(slotDisconnected()));
    connect(widgetClient, SIGNAL(signalProgressBarReset()),
            progressBar, SLOT(slotReset()));
    connect(widgetClient, SIGNAL(signalProgressBarSetRange(int)),
            progressBar, SLOT(slotInit(int)));
    connect(widgetClient, SIGNAL(signalProgressBarNextStep()),
            progressBar, SLOT(slotNextStep()));
    connect(widgetBase, SIGNAL(signalProgressBarReset()),
            progressBar, SLOT(slotReset()));
    connect(widgetBase, SIGNAL(signalProgressBarSetRange(int)),
            progressBar, SLOT(slotInit(int)));
    connect(widgetBase, SIGNAL(signalProgressBarNextStep()),
            progressBar, SLOT(slotNextStep()));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
