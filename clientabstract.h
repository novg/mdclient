#ifndef CLIENTABSTRACT_H
#define CLIENTABSTRACT_H

#include "tcpclient.h"
#include "connectdata.h"
#include <QMainWindow>
#include <QQueue>
#include <QFile>

class ClientAbstract : public QMainWindow
{
    Q_OBJECT
public:
    explicit ClientAbstract(QWidget *parent = 0);

public:
    void setConnectData(const ConnectData *connectData);
    void runQueueCommands(const QQueue<QString> &queCommand);

signals:
    void signalRequireCommands();
    void signalConnected();
    void signalDisconnected();
    void signalProgressBarReset();
    void signalProgressBarSetRange(const int &num);
    void signalProgressBarNextStep();

public slots:

protected slots:
    void slotConnected();
    void slotReceiveFromServer(QString &text);
    void slotDisconnected();

private slots:
    virtual void slotQueueCommandsEmpty() = 0;

protected:
    void createLogDir();
    void openLogFile(QFile::OpenMode openMode);
    void createConnect();

protected:
    QString pathLog;
    TcpClient *mdclient;
    QString address;
    QString port;
    QFile logfile;
};

#endif // CLIENTABSTRACT_H
