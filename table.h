#ifndef TABLE_H
#define TABLE_H

#include <QObject>
#include <QSqlQuery>

//class QSqlQuery;

class Table : public QObject
{
    Q_OBJECT
public:
    explicit Table(const QString &tableName, QSqlQuery &sqlQuery, QObject *parent = 0);

public:
    bool create(const QString &strSql);
    bool insert(const QStringList &values);
    void setSqlInsert(const QString &str);
    bool clearTable();

signals:

public slots:

private:
    QString name;
    QString sqlInsert;
    QSqlQuery query;
};

#endif // TABLE_H
