#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QDataStream>
#include <QDebug>
#include <QQueue>

class TcpClient : public QObject
{
    Q_OBJECT
public:
    explicit TcpClient(QObject *parent = 0);
    ~TcpClient();

public:
    void connectToHost(const QString &host, const QString &port);
    void setConnectData(const QString &user, const QString &passwordString);
    void setQueueCommands(const QQueue<QString> &commands);
    void emptyQueue();

signals:
    void signalMessage(QString &str);
    void signalConnected();
    void signalDisconnected();
    void signalReceiveFromServer(QString &text);
    void signalQueueCommandsEmpty();
    void signalProgressBarNextStep();

public slots:
    void slotDisconnectFromHost();

private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError error);
    void slotReceiveFromServer(QString &text);

private:
    void sendToServer(const QString &text);
    void createConnect();
    void runCommands();

private:
    bool authorized;
    QTcpSocket *socket;
    QByteArray  buffer;
    QString userName;
    QString password;
    QQueue<QString> queCommand;
};

#endif // TCPCLIENT_H
